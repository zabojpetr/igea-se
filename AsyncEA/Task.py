import datetime
from enum import Enum
from AsyncEA import const

class Terminated(Enum):
    NONE = 1
    REGULAR = 2
    BAD = 3
    CROWDED = 4
    BAD_POSITION = 5
    BAD_TOURNAMENT = 6
    TRANSFER = 7

class Speculative(Enum):
    NONE = 1
    UNKNOWN = 2
    GOOD = 3
    BAD = 4

class Task:
    def __init__(self, id, speculative, occurrences, generation, individual):
        self.id = id
        self.speculative = speculative
        self.occurrences = occurrences
        self.generation = generation
        self.individual = individual
        self.start_time = datetime.timedelta(seconds=-1)
        self.end_time = datetime.timedelta(seconds=-1)
        self.cpu = -1
        self.value = ()
        self.terminate_time = datetime.timedelta(seconds=-1)
        self.terminated = Terminated.NONE

    def priority(self):
        # priority in min-heap
        # order: speculative, min gen, max occurences
        # good-speculative tasks has same priority as non-speculative tasks

        # priorita v min halde
        # speculative, min gen, max occurences
        # dobre spekulativni tasky maji stejnou prioritu jako normalni taky
        return [self.speculative.value if self.speculative != Speculative.GOOD else Speculative.NONE.value, self.generation, -self.occurrences, self.id]

    def renew(self):
        self.start_time = datetime.timedelta(seconds=-1)
        self.end_time = datetime.timedelta(seconds=-1)
        self.cpu = -1
        self.value = ()
        self.terminate_time = datetime.timedelta(seconds=-1)
        self.terminated = Terminated.NONE

    def __lt__(self, other):
        return self.id < other.id

    def __repr__(self):
        return f"id: {self.id}, speculative: {str(self.speculative).split('.')[-1]}, gen: {self.generation}, value: {self.value}, individual: {self.individual}"