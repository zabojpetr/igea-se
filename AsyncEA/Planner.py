from collections import namedtuple
from enum import Enum
import heapq
import logging
import datetime
from AsyncEA.Logging import StatsLog
from AsyncEA.Task import *
import random

# Task = namedtuple('Task', 'args generation occurrences speculative')
# RunningTask = namedtuple('RunningTask', ['end_time', 'cpu', 'value', 'args', 'start_time'])
# TaskResult = namedtuple('TaskResult', ['value', 'args', 'generation', 'occurrences'])
PriorityTask = namedtuple('PriorityTask', ['priority', 'task'])

class Abortable(Enum):
    YES = 1
    ONLY_BAD_TASKS = 2
    NO = 3



class Planner:
    def __init__(self, parallel_pool, use_estimate, abortable):
        self.parallel_pool = parallel_pool
        self.use_estimate = use_estimate
        self.abortable = abortable

        self.tasks = []
        self.running_tasks = []
        self.finished_tasks = []

        self.finished_good_tasks = 0
        self.finished_not_good_tasks = 0

        self.log = None

        self.logger = logging.getLogger(__name__)
        self.logger.info(f"Planner initialize: Parallel pool={type(parallel_pool)}(cpu={parallel_pool.n_cpus}), Speculative evaluation={use_estimate}, Aborting tasks={abortable}")

        self._started = False

    def add(self, task:Task):
        if(not self._started):
            if not self.log:
                self.log = StatsLog(name_postfix="_planner", weights=task.individual.fitness.weights, planner=self)
            self._started = True

        heapq.heappush(self.tasks, PriorityTask(task.priority(), task))

        self.logger.debug(f"Task added: Priority={task.priority()}, {task}")

        self._start_next_tasks()

    def remove(self, task:Task, terminate_type:Terminated):
        self.logger.debug(f"Remove task: {task}")
        if task.speculative != Speculative.NONE and task.speculative != Speculative.GOOD:
            removed = False

            # Remove from planned tasks
            # Odebrani z naplanovanych tasku
            task_to_remove = [(idx, t) for idx,t in enumerate(self.tasks) if t.task.id == task.id]
            if task_to_remove:
                # move the last item in list (heap) to the position of removed item
                # přiřazení posledního prvku listu (haldy) na místo vyřazeního prvku
                self.tasks[task_to_remove[0][0]] = self.tasks[-1]
                # remove last item from list
                # odstranění posledního prvku z listu
                self.tasks.pop()
                # O(n)
                heapq.heapify(self.tasks)
                # O(log(n))
                # if i < len(h):
                #     heapq._siftup(h, i)
                #     heapq._siftdown(h, 0, i)
                removed = True

                self.logger.debug(f"Removed submitted task: {task_to_remove[0][1]}")

            # remove from running tasks
            # Odebrani z bezicich tasku
            if self.abortable == Abortable.YES or (self.abortable == Abortable.ONLY_BAD_TASKS and terminate_type == Terminated.BAD):
                task_to_remove = [(idx,t) for idx,t in enumerate(self.running_tasks) if t.id == task.id]
                if task_to_remove:
                    self.parallel_pool.terminate(task_to_remove[0][1], terminate_type)
                    if task_to_remove[0][1].cpu in self.parallel_pool.free_cpus:
                        self.running_tasks.remove(task_to_remove[0][1])
                        removed = True

                        self.log.planner_log(task_to_remove[0][1])
                        self.logger.debug(f"Removed running task: {task_to_remove[0][1]}")
                    else:
                        self.logger.debug(f"Abort failed for running task: {task_to_remove[0][1]}")
            else:
                self.logger.debug(f"Not possible abort task, because {self.abortable} and terminate_type={terminate_type}")

        else:
            self.logger.warning(f"Only a spectaculative task can be removed!")

        self._start_next_tasks()


    def all_evaluations_finished(self):
        return len(self.parallel_pool.free_cpus) == self.parallel_pool.n_cpus and not self.tasks

    def next_finished(self):
        task = self.parallel_pool.next_finished()
        # values update
        # aktualizace hodnot
        running_task = [x for x in self.running_tasks if x.id == task.id]
        task.speculative = running_task[0].speculative
        if task not in self.running_tasks:
            print(task)
        # diffrent referent of object after returning from another process
        # jiny objekt po behu v jinem procesu
        self.running_tasks.remove(running_task[0])
        self._start_next_tasks()
        self.finished_tasks.append(task)

        self.log.planner_log(task)
        self.logger.debug(f"Next finished task: {task}")

        if task.speculative == Speculative.NONE or task.speculative == Speculative.GOOD:
            self.finished_good_tasks += 1
        else:
            self.finished_not_good_tasks += 1

        return task

    def free_cpus(self):
        return self.parallel_pool.free_cpus

    def stats(self):
        print(f"Running tasks/cpus: {self.running_tasks}/{self.parallel_pool.n_cpus}")
        print(f"Time: {self.parallel_pool.time()}")

    def evals(self):
        return self.finished_good_tasks

    def _start_next_tasks(self):
        abortable_tasks = [x for x in self.running_tasks if x.speculative.value >= Speculative.UNKNOWN.value and x.speculative != Speculative.GOOD]
        # number of non-speculative tasks is greater than number of free CPUs
        # and we can abort tasks and in running tasks there are samo speculative tasks
        # nespekulativních tasků je víc než volných procesorů
        # a nastavení abortování na ANO a vyhodnocujeme nějaké spekulativní tasky
        while len(self.parallel_pool.free_cpus) < len([x for x in self.tasks if x.task.speculative == Speculative.NONE]) \
                and self.abortable == Abortable.YES \
                and abortable_tasks:
            max_priority = max(x.priority() for x in abortable_tasks)
            # remove from the highest priority (run the shortest time)
            # odstraňovat od nejvyšší priority (běží nejkratší dobu)
            task = next(x for x in abortable_tasks if x.priority() == max_priority)

            self.remove(task, Terminated.REGULAR)

            abortable_tasks.remove(task)
            # add to the queue of tasks again
            # pridani zpet do fronty tasku
            task.renew()
            heapq.heappush(self.tasks, PriorityTask(task.priority(), task))

        while(self.parallel_pool.free_cpus and self.tasks):
            priority_task = heapq.heappop(self.tasks)
            self.running_tasks.append(priority_task.task)

            self.logger.debug(f"Adding task to pool: {priority_task.task}")
            self.parallel_pool.apply_async(priority_task.task)
