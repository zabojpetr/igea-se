import datetime
import os
import itertools
from deap import tools
from collections import namedtuple
from enum import Enum

# All logs from one run will have same time
# Všechny logy z jednoho běhu budou mít stejný čas
LOG_DATETIME = None
LOG_DIR = None

def create_log_name(dir=None, date=None, postfix="", extension=None):
    """
    Create name of log file
    :param date:
    :param extension:
    :return: log filename
    """

    global LOG_DIR
    global LOG_DATETIME
    if date is None:
        date = time_for_log()
    else:
        LOG_DATETIME = date
    if dir is None:
        if LOG_DIR == None:
            dir = "logs"
        else:
            dir = LOG_DIR
    else:
        LOG_DIR = dir

    if isinstance(date, int):
        name = "log-" + str(date) + postfix + ("" if extension is None else ("." + extension))
    else:
        name = "log-" + date.strftime('%Y%m%d-%H%M%S') + postfix + ("" if extension is None else ("." + extension))
    return os.path.join(dir, name)




def time_for_log():
    global LOG_DATETIME
    if LOG_DATETIME is None:
        LOG_DATETIME = datetime.datetime.now()
    return LOG_DATETIME


class Generation:
    def __init__(self, parents_size, offsprings_size, tournament_aspirants, pop = None, selected = None, offsprings = None, aspirants = None, waiting = None):
        self.tournament_aspirants = tournament_aspirants                                                # pirs to tournament - pary jedincu do turnaje
        self.pop = pop or [None]*parents_size                                                           # population (parents) - populace (rodice)
        self.selected = selected or [None]*(offsprings_size+offsprings_size % 2)                        # tournament winners (mating pool) - jedinci po turnaji, pripraveni na krizeni
        self.offsprings = offsprings or [None]*offsprings_size                                          # offsprings - potomci
        self.aspirants = aspirants or set(itertools.chain.from_iterable(tournament_aspirants))          # individuals which are in aspirant pairs, we need evaluate them - jedinci, ktere je potreba ohodnotit, protoze jsou v aspirujicich parech
        self.waiting = waiting or set()                                                                 # individuals which are not evaluated yet - jedinci, kteri jeste nebyli ohodnoceni
        self.invalid_ind = 0                                                                            # number of evaluated individuals (offsprings) in the generation - pocet vyhodnocovanych jedincu (potomku) v generaci
        self.in_pop = set()                                                                             # offsprings, that we know, will step forward to the next generation and we already include them there - potomci o kterych vime ze jsou v dalsi generaci a uz jsme je tam zaradili

        self.speculative = set()                                                                        # individuals which we added to the speculative parent from non-speculative offsprings - jedinci, ktere jsme pridali do spekulativniho vyhodnocovani
        self.speculative_pop = [None]*parents_size                                                      # speculative individuals like pop - spekulativni jedinci, na pozici jako v pop
        self.speculative_selected = [None]*(offsprings_size+offsprings_size % 2)                        # speculative tournament winners (speculative mating pool) - spekulativni jedinci po turnaji (mozna po spekulativnim turnaji), pripraveni na krizeni
        self.speculative_offsprings = [None]*offsprings_size                                            # speculative offsprings (one of parent is speculative) - spekulativni potomci (jeden z rodicu je spekulativni nebo aktualne pridany)
        self.speculative_in_pop = set()                                                                 # speculative version of in_pop - spekulativni jedinci pridani do dalsi generace
        self.speculative_tournament = [None]*(offsprings_size+offsprings_size % 2)                      # transition table for tournament (parent position -> mating pool position) - Tranzitni tabulka pro turnaj
        self.speculative_cross = set()                                                                  # transition table for genetic operators (mating pool -> offsprings)
        self.speculative_invalid_ind = 0                                                                # number of wrong speculative individuals
        self.speculative_waiting = set()                                                                # speculative version of waitings
        self.speculative_tournament_valid = [False]*(offsprings_size+offsprings_size%2)                 # information about validity of tournament


def generate_aspirant_pairs(parents_size, offsprings_size):
    pop_idx = list(range(parents_size))
    return [tools.selRandom(pop_idx, 2) for _ in range(offsprings_size+offsprings_size % 2)]



class TransitionType(Enum):
    SPECULATIVE = 0
    PARENT = 1
    TOURNAMENT = 2
    CROSS = 3
    IN_POP = 4


class Source(Enum):
    PARENT = 0
    OFFSPRING = 1
    SPECULATIVE_PARENT = 2
    SPECULATIVE_OFFSPRING = 3


class EAType(Enum):
    COMMA = 0
    PLUS = 1
    CLEVER_TOURNAMENT = 2



class Transition:
    def __init__(self, source=None, source_index=None, source_list_index=None, destination_index=None, destination_list_index=None):
        self.source = source
        self.source_index = source_index
        self.source_list_index = source_list_index
        self.destination_index = destination_index
        self.destination_list_index = destination_list_index

    class Mask(Enum):
        ALL = 1
        SOURCE = 2
        SPECULATIVE_SOURCE = 3
        DESTINATION = 4
        SPECULATIVE_DESTINATION = 5
        NONE = 6


    def in_collection(self, collection, mask):
        if isinstance(mask, Transition):
            pass
        elif mask == self.Mask.ALL:
            mask = Transition(True, True, True, True, True)
        elif mask == self.Mask.SOURCE:
            mask = Transition(True, True, False, False, False)
        elif mask == self.Mask.SPECULATIVE_SOURCE:
            mask = Transition(True, True, True, False, False)
        elif mask == self.Mask.DESTINATION:
            mask = Transition(False, False, False, True, False)
        elif mask == self.Mask.SPECULATIVE_DESTINATION:
            mask = Transition(False, False, False, True, True)
        elif mask == self.Mask.NONE:
            mask = Transition(False, False, False, False, False)
        else:
            mask = Transition(True, True, True, True, True)

        finded = []
        if collection is None:
            return finded
        for x in collection:
            if ((not mask.source or x.source == self.source)
                    and (not mask.source_index or x.source_index == self.source_index)
                    and (not mask.source_list_index or x.source_list_index == self.source_list_index)
                    and (not mask.destination_index or x.destination_index == self.destination_index)
                    and (not mask.destination_list_index or x.destination_list_index == self.destination_list_index)):
                finded.append(x)
        return sorted(finded)

    def __repr__(self):
        return f"S: {str(self.source).split('.')[-1]}, " \
               f"SI: {str(self.source_index)}, " \
               f"SLI: {str(self.source_list_index)}, " \
               f"DI: {str(self.destination_index)}, " \
               f"DLI: {str(self.destination_list_index)}"

    def __lt__(self, other):
        if self.source.value == other.source.value:
            if self.source_index == other.source_index:
                if self.source_list_index == other.source_list_index:
                    if self.destination_index == other.destination_index:
                        if self.destination_list_index == other.destination_list_index: # if there is None
                            return False
                        else:
                            return self.destination_list_index < other.destination_list_index
                    else:
                        return self.destination_index < other.destination_index
                else:
                    return self.source_list_index < other.source_list_index
            else:
                return self.source_index < other.source_index
        else:
            return self.source.value < other.source.value