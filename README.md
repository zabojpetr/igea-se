# IGEA-SE

Parallel evolutionary algorithm with interleaving generations and speculative evaluation.

Authors: Petr Záboj, Martin Pilát 
Contact: petr.zaboj@gmail.com, [Martin.Pilat@mff.cuni.cz](mailto:Martin.Pilat@mff.cuni.cz) 
GitLab Repository: https://gitlab.com/zabojpetr/igea-se

## Archive content

This archive contains source files used to create the results in the paper in the form in which they were used (without configurations of runs in yaml files).

The main file `simulation.py` runs all experiments configured in the configuration yaml file (like `settings.yml`) given as parameter.

The file `environment.yml` is the creation file for Anaconda environment.



The directory `AsyncEA` forms module, where there is implemented the main algorithm.

The module contains:

The `algorithm.py` file contains the implementation of all algorithms described in paper. The implementations are mostly compatible with deap library.

The `const.py` file stores constants and translation tables.

The `fitness_functions.py` contains all used fitness functions.

The `Logging.py` file implements the logging of algorithms.

The `LogProcessing.py` file implements the log summarization from the `Logging.py` file.

The `ParallelSimulator.py` contains the simulator of the parallel computer.

The `Planner.py` file plans the order of individuals, in which they will be evaluated.

The `PlottingGraph.py` file creates plots from `LogProcessing` output. It can create the fitness plot, utilization plot and plot which shows how many individuals from each generation are evaluated in
each time.

The `SurrogateModels.py` contains the surrogate model abstract class and the implementation of surrogate model used in experiments.

The `Taks.py` file wraps information about the computation task. It contains the individual, start time, end time, ... 

The `time_functions.py` contains all time functions used in experiments.

The `utils.py` contains few utilities needed in experiments.

## How to use

First we create Anaconda environment:

`conda env create -f environment.yml`

Then we activate environment:

`activate igea_se`

Finally, we run simulation:

`python simulation.py settings.yml`