import random

def makeRegistrar():
    registry = {}
    def registrar(func):
        registry[func.__name__] = func
        return func  # normally a decorator returns a wrapped function,
                     # but here we return func unmodified, after registering it
    registrar.all = registry
    return registrar

tf = makeRegistrar()

@tf
def constant_time_function(value, ind, *args):
    return 1


@tf
def random10_time_function(value, ind, *args):
    return 1 + random.random() * 9

@tf
def random100_time_function(value, ind, *args):
    return 1 + random.random() * 99


@tf
def random1000_time_function(value, ind, *args):
    return 1 + random.random() * 999


@tf
def exponential_time_function(value, ind, *args):
    return random.expovariate(1.0)


@tf
def positive_corelated_time_function(value, ind, *args):
    return 1 + value[0]


@tf
def negative_corelated_time_function(value, ind, *args):
    return 1 + max(100-value[0], 0)