import heapq
from multiprocessing import Process, Queue, Manager
import datetime
import logging
from collections import namedtuple
from AsyncEA.Task import *

RunningProcess = namedtuple('RunningProcess', ['process', 'running_task'])
RunningSimulatorProcess = namedtuple('RunningSimulatorProcess', ['end_time', 'running_task'])

class ParallelSimulator:
    def __init__(self, n_cpus, eval_func, time_func):
        self.n_cpus = n_cpus
        self.eval_func = eval_func
        self.time_func = time_func

        self.running_tasks = []
        self.current_time = datetime.timedelta(seconds=0)
        self.free_cpus = set(range(n_cpus))

    def apply_async(self, task):
        task.start_time = self.current_time
        task.cpu = self.free_cpus.pop()
        task.value = self.eval_func(task.individual)
        task.end_time = datetime.timedelta(seconds=self.time_func(task.value, task.individual)) + self.current_time
        heapq.heappush(self.running_tasks, RunningSimulatorProcess(task.end_time, task))

    def terminate(self, task, terminate_type):
        # we do not make simulation with waiting for termination, but we set terminate_time = end_time = current_time
        # (due to logs)
        # nebudeme dělat simulaci s čekáním, ale nastavíme terminate time = end time = current time (kvůli logům)
        terminated = next(iter([x for x in self.running_tasks if x.running_task.id == task.id]), None)
        if terminated == None:
            return None

        self.running_tasks.remove(terminated)
        heapq.heapify(self.running_tasks)
        self.free_cpus.add(terminated.running_task.cpu)

        terminated.running_task.terminate_time = self.current_time
        terminated.running_task.terminated = terminate_type
        terminated.running_task.end_time = self.current_time
        return terminated.running_task

    def next_finished(self):
        finished = heapq.heappop(self.running_tasks).running_task
        self.current_time = finished.end_time
        self.free_cpus.add(finished.cpu)
        return finished

    def time(self):
        return self.current_time


class ParallelPool:
    def __init__(self, n_cpus, eval_func):
        self.n_cpus = n_cpus
        self.eval_func = eval_func

        self.running_tasks = []
        self.finished_tasks = Manager().Queue()
        self.free_cpus = set(range(n_cpus))
        self.start_time = datetime.datetime.now()

        self._started = False

    def apply_async(self, task):
        if not self._started:
            self.start_time = datetime.datetime.now()
            self._started = True

        task.start_time = datetime.datetime.now() - self.start_time
        task.cpu = self.free_cpus.pop()

        p = Process(target=self._fitness_function, args=(task,))

        p.daemon = True
        self.running_tasks.append(RunningProcess(p, task))
        p.start()

        # self.running_tasks.append(RunningProcess(p, task))
        # self._fitness_function(task)

    def terminate(self, task, terminate_type):
        terminated = next(iter([x for x in self.running_tasks if x.running_task.id == task.id]), None)
        if terminated == None:
            return None

        self.running_tasks.remove(terminated)
        self.free_cpus.add(terminated.running_task.cpu)

        terminated.process.terminate()
        terminated.running_task.terminate_time = datetime.datetime.now() - self.start_time
        terminated.running_task.terminated = terminate_type
        return terminated.running_task

    def next_finished(self):
        task = self.finished_tasks.get()
        running_task = next(iter([x for x in self.running_tasks if x.running_task.id == task.id]), None)
        if running_task == None:
            logging.debug("Task get from finished_tasks not found in running_tasks")
        else:
            self.running_tasks.remove(running_task)
        self.free_cpus.add(task.cpu)

        return task

    def _fitness_function(self, task):
        task.value = self.eval_func(task.individual)
        task.end_time = datetime.datetime.now() - self.start_time
        self.finished_tasks.put(task)

    def time(self):
        return datetime.datetime.now() - self.start_time
