import math

def makeRegistrar():
    registry = {}
    def registrar(func):
        registry[func.__name__] = func
        return func  # normally a decorator returns a wrapped function,
                     # but here we return func unmodified, after registering it
    registrar.all = registry
    return registrar

ff = makeRegistrar()

@ff
def constant_function(ind, *args):
    return 1,


@ff
def rastrigin_function(ind, *args):
    return 10*len(ind) + sum(map(lambda xi: xi*xi - 10*math.cos(2*math.pi*xi), ind)),

