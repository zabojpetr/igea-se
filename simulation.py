from AsyncEA import algorithms, Planner, ParallelSimulator, LogProcessing, PlottingGraph, utils, SurrogateModels
from deap import base, creator, tools
import random
import logging
import numpy as np
import math
import datetime

import pdb, traceback, sys, code
import yaml
import argparse
import os
from subprocess import Popen, PIPE
import re
import platform
import shutil
import distutils
import distutils.dir_util

def my_import(name):
    parts = name.split('.')
    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m

def create_toolbox(ff):
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register("attr_float", random.random)
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_float, n=IND_SIZE)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    toolbox.register("evaluate", ff)
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=SIGMA, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=2)

    return toolbox

def compress_data(id, alg, n_cpus, tf_func, ff_func, speculative, abortable, config):
    alg_name = alg
    if alg.startswith("igea"):
        alg_name += f"_{speculative}_{abortable.name}"

    tmp_log_dir_path = os.path.abspath(config["logs"]["tmp_log_dir_path"])
    log_dir_path = os.path.abspath(config["logs"]["log_dir_path"])
    logs = os.path.join(alg_name, tf_func.__name__, ff_func.__name__)
    tmp_directory = os.path.join(tmp_log_dir_path, id, logs)
    directory = os.path.join(log_dir_path, logs)
    os.makedirs(directory, exist_ok=True)

    try:
        if platform.system() == "Windows":
            tar = Popen([r"C:\Program Files\Bandizip\bc.exe", "c", os.path.join(directory, str(n_cpus) + ".zip"),
                         os.path.join(tmp_directory, str(n_cpus))], stdin=PIPE,
                        stdout=PIPE, stderr=PIPE)
        else:
            tar = Popen(["tar", "-C", tmp_directory, "-czf", os.path.join(directory, str(n_cpus) + ".tar.gz"), str(n_cpus)], stdin=PIPE,
                        stdout=PIPE, stderr=PIPE)
        tar.wait()


        shutil.rmtree(tmp_directory)
        print("Compressed: " + tmp_directory)
    except:
        distutils.dir_util.copy_tree(tmp_directory, directory)
        shutil.rmtree(tmp_directory)
        print("Compress failed: " + tmp_directory)

def run_alg(id, alg, n_cpus, i, tf_func, ff_func, speculative, abortable, config):
    try:
        alg_name = alg
        if alg.startswith("igea"):
            alg_name += f"_{speculative}_{abortable.name}"


        seed = i+1
        random.seed(seed)
        np.random.seed(seed)

        surrogate_model = my_import(config["surrogate_model"]["class"])(**config["surrogate_model"]["kwargs"])
        toolbox = create_toolbox(ff_func)

        sim_class = my_import(config["simulator"])
        try:
            sim = ParallelSimulator.ParallelSimulator(n_cpus, toolbox.evaluate, tf_func)
        except:
            sim = ParallelSimulator.ParallelSimulator(n_cpus, toolbox.evaluate)
        planner = Planner.Planner(sim, speculative, abortable)

        log_dir_path = config["logs"]["tmp_log_dir_path"]
        directory = os.path.join(log_dir_path, id, alg_name, tf_func.__name__, ff_func.__name__, str(n_cpus))
        utils.create_log_name(directory, i)


        print(f"{datetime.datetime.now()}, alg: {alg_name}, tf: {tf_func.__name__}, ff: {ff_func.__name__}, cpus: {n_cpus}, run: {i}")

        # verbose = __debug__
        verbose = config["logs"]["verbose"]
        if alg == "ea_comma":
            algorithms.ea_comma(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, OFF_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, verbose=verbose)
        elif alg == "ea_plus":
            algorithms.ea_plus(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, OFF_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, verbose=verbose)
        elif alg == "ea_clever_tournament":
            algorithms.ea_clever_tournament(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, verbose=verbose)
        elif alg == "async_ea":
            algorithms.async_ea(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, CXPB, MUTPB, MAX_EVAL, verbose=verbose)
        elif alg == "igea_comma":
            algorithms.igea_comma(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, OFF_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, surrogate_model=surrogate_model, verbose=verbose)
        elif alg == "igea_plus":
            algorithms.igea_plus(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, OFF_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, surrogate_model=surrogate_model, verbose=verbose)
        elif alg == "igea_clever_tournament":
            algorithms.igea_clever_tournament(toolbox.population(POP_SIZE), toolbox, planner, POP_SIZE, CXPB, MUTPB, MAX_GEN, MAX_EVAL, surrogate_model=surrogate_model, verbose=verbose)

        if config["logs"]["summary"]:
            logname = utils.create_log_name(postfix="_planner")
            l = LogProcessing.LogProcessing.from_log_file(logname + ".txt")
            l.create_summary()
            if config["logs"]["one_run_plot"]:
                p = PlottingGraph.PlottingGraph.from_summary_file(logname + "_summary.json")
                p.cpu_used_graph()

    except Exception as e:
        print(f"Chyba: {datetime.datetime.now()}, alg: {alg_name}, tf: {tf_func.__name__}, ff: {ff_func.__name__}, cpus: {n_cpus}, run: {i}")
        print(e)
        traceback.print_exc()


parser = argparse.ArgumentParser(description='AsyncEA simulation.')
parser.add_argument('config', type=str, help='Path to config file.')
args = parser.parse_args()

with open(args.config, 'r') as fs:
    try:
        config = yaml.safe_load(fs)
    except yaml.YAMLError as exc:
        print(exc)

seed = config["ea_settings"]["main_seed"]
random.seed(seed)
np.random.seed(seed)

IND_SIZE = config["ea_settings"]["ind_size"]
POP_SIZE = config["ea_settings"]["pop_size"]
OFF_SIZE = config["ea_settings"]["off_size"]
MAX_EVAL = config["ea_settings"]["max_eval"]
MAX_GEN = config["ea_settings"]["max_gen"]

CXPB = config["ea_settings"]["cxpb"]
MUTPB = config["ea_settings"]["mutpb"]
SIGMA = config["ea_settings"]["sigma"]

REPETITION = config["ea_settings"]["repetition"]

tfs = []
for tf in config["tfs"]:
    tfs.append(my_import(tf))

ffs = []
for ff in config["ffs"]:
    ffs.append(my_import(ff))

algs = config["algs"]

abortables = []
config["abortables"] = config["abortables"] if config["abortables"] else []
for abortable in config["abortables"]:
    abortables.append(Planner.Abortable[abortable])

speculatives = []
for speculative in config["speculatives"]:
    speculatives.append(speculative)

n_cpus = config["n_cpus"]

id = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')

os.makedirs(os.path.join(config["logs"]["tmp_log_dir_path"]), exist_ok=True)
output_name = (id+"_"+args.config).replace("/", "_").replace(".","_")+".txt"
fs = open(os.path.join(config["logs"]["tmp_log_dir_path"], output_name), 'w')
sys.stdout = fs
sys.stderr = fs

start_total_time = datetime.datetime.now()
start_time = datetime.datetime.now()
for alg in algs:
    for tf_func in tfs:
        for ff_func in ffs:
            for n_cpu in n_cpus:
                for i in range(REPETITION):
                    try:
                        kinit = Popen(["kinit", "-R"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
                        kinit.wait()
                    except:
                        print("[ERROR] Kinit failed!")
                    #pokud je igea chceme navic povolit spekulativni a vyzkouset ruzne odstraneni
                    if (alg.startswith("igea")) and (True in speculatives):
                        for abortable in abortables:
                            run_alg(id, alg, n_cpu, i, tf_func, ff_func, True, abortable, config)
                            if i+1 == REPETITION:
                                compress_data(id, alg, n_cpu, tf_func, ff_func, True, abortable, config)

                    if not alg.startswith("igea") or (alg.startswith("igea") and False in speculatives):
                        run_alg(id, alg, n_cpu, i, tf_func, ff_func, False, Planner.Abortable.NO, config)
                        if i+1 == REPETITION:
                            compress_data(id, alg, n_cpu, tf_func, ff_func, False, Planner.Abortable.NO, config)



    print("alg time: " + str(datetime.datetime.now() - start_time))
    print("total time: " + str(datetime.datetime.now() - start_total_time))
    start_time = datetime.datetime.now()
    for i in range(10): print()

fs.flush()
fs.close()

shutil.move(os.path.join(config["logs"]["tmp_log_dir_path"], output_name), os.path.join(config["logs"]["log_dir_path"], output_name))