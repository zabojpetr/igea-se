from operator import itemgetter

from deap import tools, algorithms
from AsyncEA import utils, Task, Logging, ParallelSimulator, const
from AsyncEA.utils import Source, Transition, TransitionType, EAType
from collections import Counter
import sys
import random
import numpy as np
import copy
import itertools
import heapq
import os


def dump_log(gens, res, planner, reset_sub_step = False):
    """
    Write all variables to files
    Each file is folded from blocks
    The block is matrix individual×generation

    Zapíše vše do souboru
    soubor z bloků
    blok je matice jedinec×generace
    """
    global fw
    global dmp_names
    global step
    global sub_step
    if 'fw' not in globals():
        step = 0
        sub_step = 0
        dmp_names = [
            "tournament_aspirants",
            "pop",
            "selected",
            "offsprings",
            "aspirants",
            "waiting",
            "in_pop",
            "speculative",
            "speculative_pop",
            "speculative_selected",
            "speculative_offsprings",
            "speculative_in_pop",
            "speculative_tournament",
            "speculative_cross",
            "speculative_waiting",
            "speculative_tournament_valid"
        ]
        fw = dict()
        date = utils.time_for_log()
        if isinstance(date, int):
            path = os.path.join("dump", str(date))
        else:
            path = os.path.join("dump/", date.strftime('%Y%m%d-%H%M%S'))
        ext = ".csv"
        for name in dmp_names:
            fn = os.path.join(path, name+ext)
            if not os.path.exists(path):
                os.makedirs(path)
            fw[name] = open(fn, "w")

        fn = os.path.join(path, "ind" + ext)
        fw["ind"] = open(fn, "w")

        fn = os.path.join(path, "tasks" + ext)
        fw["tasks"] = open(fn, "w")

    if reset_sub_step:
        fw["ind"].write(f"id:{res.id}|speculative:{res.speculative.name}|gen:{res.generation}|ind:{res.individual.idx}"
                        +f"|{(str(res.individual.fitness.wvalues[0]) if res.individual.fitness.valid else '')}")
        fw["ind"].write("\n")
        fw["ind"].write(str(step).zfill(5) + "#"*75 + "\n")
        # fw["ind"].flush()

    for r in planner.running_tasks:
        fw["tasks"].write(f"{r.id}|{r.generation}|{r.speculative.name};")
    fw["tasks"].write("\n")
    for pt in planner.tasks:
        r = pt.task
        fw["tasks"].write(f"{r.id}|{r.generation}|{r.speculative.name};")
    fw["tasks"].write("\n")
    fw["tasks"].write(str(step).zfill(5) + "-" + str(sub_step).zfill(3) + "-" + "#"*70 + "\n")
    # fw["tasks"].flush()

    for name in dmp_names:
        m = 0
        for g in range(len(gens)):
            m = max(m, len(getattr(gens[g], name)))
        for i in range(m):
            for g in range(len(gens)):
                if i >= len(getattr(gens[g], name)):
                    fw[name].write(" ;")
                    continue
                if isinstance(getattr(gens[g], name), list):
                    item = getattr(gens[g], name)[i]
                else:
                    item = sorted(getattr(gens[g], name))[i]
                if isinstance(item, list) and hasattr(item, "fitness"): #individuum
                    idx = getattr(item, "idx") if hasattr(item, "idx") else "-1"
                    id = getattr(item, "id") if hasattr(item, "id") else "-1"
                    fitness = getattr(item, "fitness")
                    fw[name].write(str(idx) + "_" + str(id) + ":" + (str(fitness.wvalues[0]) if fitness.valid else ""))
                    # fw[name].write("|".join(map(str,item)))
                elif isinstance(item, list) and item and isinstance(item[0], list) and hasattr(item[0], "fitness"):
                    for it in item:
                        idx = getattr(it, "idx") if hasattr(it, "idx") else "-1"
                        id = getattr(it, "id") if hasattr(it, "id") else "-1"
                        fitness = getattr(it, "fitness")
                        fw[name].write(str(idx) + "_" + str(id) + ":"
                                       + (str(fitness.wvalues[0]) if fitness.valid else "") + "|")
                elif isinstance(item, list):
                    fw[name].write("|".join(map(str, item)))
                else:
                    fw[name].write(str(item))
                fw[name].write(";")
            fw[name].write("\n")
        fw[name].write(str(step).zfill(5) + "-" + str(sub_step).zfill(3) + "-" + "#"*70 + "\n")
        # fw[name].flush()

    if reset_sub_step:
        step+=1
        sub_step = 0
    else:
        sub_step+=1




########################################################################################################################
##                                                                                                                    ##
##                                                        IGEA                                                        ##
##                                                                                                                    ##
########################################################################################################################
#region IGEA FUNCTIONS
def igea(ea_type, population, toolbox, planner, mu, lambda_, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0, stats=None,
             halloffame=None, surrogate_model=None, verbose=__debug__):
    assert max_gen or max_evals, "At least one of the max_gen or max_evals must be set!"
    assert lambda_ >= mu, "lambda must be greater or equal to mu."

    args = {}
    args[const.MU] = mu
    args[const.LAMBDA] = lambda_
    args[const.CXPB] = cxpb
    args[const.MUTPB] = mutpb
    args[const.MAX_GEN] = max_gen
    args[const.MAX_EVALS] = max_evals
    args[const.SURROGATE_MODEL] = surrogate_model.get_settings()

    algorithm = {EAType.COMMA: "igea_comma",
                 EAType.PLUS: "igea_plus",
                 EAType.CLEVER_TOURNAMENT: "igea_clever_tournament"}

    planner.log = Logging.StatsLog(name_postfix="_planner",
                                   weights=population[0].fitness.weights if population else "[]",
                                   algorithm=algorithm.get(ea_type, "igea_unknown"),
                                   planner = planner,
                                   settings=args)

    id = -mu - (lambda_ - mu)

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # In order not to have to solve the difference of the first generation, we will create a fake random generation
    # and set given population as offsprings

    # Abychom nemuseli resit rozdilnost 1. generace, vytvorime nahodnou generaci a jako potomky nastavime populaci,
    # kterou jsme dostali
    g = -1
    p0 = toolbox.population(mu)
    for ind in p0:
        ind.fitness.values = (-sys.maxsize * w for w in ind.fitness.weights)
        ind.id = id
        ind.gen = g
        id+=1

    g += 1
    o0 = toolbox.population(lambda_-mu)
    for ind in o0:
        ind.fitness.values = (-sys.maxsize * w for w in ind.fitness.weights)
        ind.id = id
        ind.gen = g
        id+=1

    o0 = o0 + population

    aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
    gens = [utils.Generation(mu, lambda_, aspirant_pairs, list(map(toolbox.clone, p0)), None, list(map(toolbox.clone, o0)))]

    aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
    gens.append(utils.Generation(mu, lambda_, aspirant_pairs))

    for idx, ind in enumerate(gens[g].offsprings):
        ind.idx = idx
        ind.gen = g
        if not ind.fitness.valid and (ea_type != EAType.CLEVER_TOURNAMENT or idx in gens[g+1].aspirants):
            gens[g].waiting.add(idx)
            t = Task.Task(id, Task.Speculative.NONE, gens[g].pop.count(ind), g, ind)
            id += 1
            gens[g].invalid_ind += 1
            planner.add(t)

    id = [id]

    # print("create transition")
    # gens[0].speculative_tournament[0] = [Transition()]
    # print("append transition")
    # gens[0].speculative_tournament[0] += [Transition()]
    # print("edit transition")
    # gens[0].speculative_tournament[0][0].source_index = 6
    # print("delete transition")
    # gens[0].speculative_tournament[0] = [x for x in gens[0].speculative_tournament[0] if x.source_index == 0]
    # print("end test")

    while (not max_evals or planner.evals() < max_evals) and (not max_gen or g < max_gen):
        # at the beginning, because when entering the while cycle I count everything what I can
        # (after the init or propagate) and then it waits for next_finished ()
        # It use information from last iterate (logicaly after igea_propagate)

        # na zacatku, protoze pri vstupu do while pocitam vse co mohu (nasleduje po initu nebo propagate)
        # a pote to ceka na next_finished()
        if planner.use_estimate:
            _igea_propagate_speculative(ea_type, id, gens, ind.gen, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model)
            counter = 0
            while len(planner.free_cpus()) != 0:
                success = _igea_create_speculative_parent(ea_type, id, gens, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model)
                _igea_propagate_speculative(ea_type, id, gens, ind.gen, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model)
                if not success:
                    break
                counter += 1
                if counter > planner.parallel_pool.n_cpus:
                    break

        result = planner.next_finished()
        ind = result.individual
        working_gen = gens[ind.gen]
        ind.fitness.values = result.value

        # print("id", str(result.id))


        # DUMP
        # dump_log(gens, result, planner, True)
        # global step
        # print(f"step: {step}")

        if result.speculative == Task.Speculative.NONE or result.speculative == Task.Speculative.GOOD:
            working_gen.offsprings[result.individual.idx].fitness.values = result.value
            working_gen.waiting.remove(ind.idx)
        else:
            l = len(working_gen.speculative_waiting)
            working_gen.speculative_waiting = set({x for x in working_gen.speculative_waiting if x[2].id != result.id})
            if l == len(working_gen.speculative_waiting):
                continue # the deleted task is returned - vracen jiz zruseny task

        _igea_propagate(ea_type, id, gens, ind.gen, toolbox, planner, mu, lambda_, cxpb, mutpb)

        if halloffame is not None:
            halloffame.update(working_gen.pop)

        # write to the logbook - in igea_propagate the next generation can be evaluated thanks to speculative evaluation
        # it can happen that the newer generation is evaluated sooner than the older one (if the individual to be waited will not be used)

        # zapsani do logbooku - v igea_propagate se muzou vyhodnotit i nasledujici generace diky spekulativnimu vyhodnoceni
        # muze dojit k tomu, novejsi generace je vyhodnocena drive nez starsi (pokud jedinec na ktereho se ceka nebude pouzit)
        for i in range(ind.gen, len(gens)):
            if len(gens[i].waiting) == 0 and None not in gens[i].offsprings:
                if logbook.select("gen") and ind.gen > logbook.select("gen")[-1]+1:
                    a=5
                if logbook.select("gen") and i in logbook.select("gen"):
                    continue

                record = stats.compile(gens[i].pop) if stats else {}
                logbook.record(gen=i, nevals=gens[i].invalid_ind, **record)
                if verbose:
                    print(logbook.stream)

                # update hall of fame too
                if halloffame is not None:
                    halloffame.update(gens[i].pop)
            else:
                break


    for task in planner.running_tasks:
        planner.log.planner_log_end(task, planner.parallel_pool.time())


def _igea_create_speculative_parent(ea_type, id, gens, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model):
    for g in range(len(gens)-1):
        working_gen = gens[g]
        next_gen = gens[g+1]

        # the generation with non-computed individuals and free positions for speculative indoviduals
        # generace s nevyhodnocenymi jedinci a volnými pozicemi pro spekulativní tasky
        if not all([x.fitness.valid for x in working_gen.offsprings if x]) \
                    and len([idx for idx in range(mu) if next_gen.pop[idx] is None and not next_gen.speculative_pop[idx]]) != 0:
            plausible_offsprings = []

            # we select non-used (but still possible good) and non-calculated individuals
            # vyberu nepouzite (ale stale mozne) a neohodnocene jedince
            evaluated_parents = [(Transition(Source.PARENT, idx), par) for idx, par in enumerate(working_gen.pop) if
                                 par and par.fitness.valid]
            evaluated_offspring = [(Transition(Source.OFFSPRING, idx), off) for idx, off in enumerate(working_gen.offsprings) if
                                   off and off.fitness.valid]
            plausible_offsprings = evaluated_offspring
            if ea_type == EAType.PLUS:
                plausible_offsprings += evaluated_parents

            plausible_offsprings = sorted(plausible_offsprings, key=lambda x: (x[1].fitness, x[0].source_index))
            # removing bad individuals and good individuals
            # odebrani spatnych a dobrych jedincu
            if ea_type != EAType.PLUS:
                plausible_offsprings = plausible_offsprings[max(len(plausible_offsprings)-mu,0):len(plausible_offsprings)-max(len(plausible_offsprings) - (lambda_ - mu), 0)]
            else:
                plausible_offsprings = plausible_offsprings[max(len(plausible_offsprings)-mu,0):len(plausible_offsprings)-max(len(plausible_offsprings) - (lambda_), 0)]

            # sort from the best to the worst by fitness and source index
            # serazeni od nejlepsiho po nejhorsiho
            plausible_offsprings = list(reversed(plausible_offsprings))

            not_evaluated_offspring = [(Transition(Source.OFFSPRING, idx), off) for idx, off in enumerate(working_gen.offsprings) if
                                   off and not off.fitness.valid]

            plausible_offsprings += not_evaluated_offspring

            # remove individuals which were selected as speculative in the past - disable, if we can add more
            # individuals to one possition

            # vyrazeni jiz vytvorenych spekulativnich tasku - zakazat pokud mohu pridavat vice tasku
            # TODO: pokud se bude pridavat vic, neodebirat pouizite tasky + zmenit CLEVER TOURNAMENT,
            #  tam vic jedincu na jednu pozici nedava smysl
            plausible_offsprings = [x for x in plausible_offsprings if not x[0].in_collection(next_gen.speculative, Transition.Mask.SOURCE)]
            if ea_type == EAType.CLEVER_TOURNAMENT:
                # remove all individuals, which are not in aspirants pairs in the next generation
                # odstraneni vsech, kteri nejsou v dalsi generaci v aspirantech
                plausible_offsprings = [x for x in plausible_offsprings if x[0].source_index in next_gen.aspirants]

            # if we use all options
            # pokud jsme pouzili vsechny moznosti
            if len(plausible_offsprings) == 0:
                continue

            selected = _select_speculative_parent(1, plausible_offsprings, gens, g, planner, toolbox, surrogate_model)

            # it cannot crash, because we check the free possition in the next_gen.speculative_pop
            # (beacause of clever tournament)

            # nemuze spadnout, protoze kontrolujeme volnou pozici v nex_gen.speculative_pop
            if ea_type == EAType.CLEVER_TOURNAMENT:
                first_free_idx = selected[0][0].source_index
            else:
                first_free_idx = next(iter([idx for idx in range(mu) if next_gen.pop[idx] is None and not next_gen.speculative_pop[idx]]))

            next_gen.speculative_pop[first_free_idx] = [x[1] for x in selected]

            for idx, x in enumerate(selected):
                x[0].destination_index = first_free_idx
                x[0].destination_list_index = idx
                next_gen.speculative.add(x[0])

            # make the tournament and genetic operators
            # provest turnaj a zaradit do offsprings
            _igea_propagate_speculative_cross_mutate(ea_type, id, gens, g, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model)

            return True
    return False


def _select_speculative_parent(number, plausible_offsprings, gens, gen, planner, toolbox, surrogate_model):
    return surrogate_model.estimate_next_parents(options = plausible_offsprings, num = number, toolbox = toolbox)


def _select_speculative_parent_2(number, plausible_offsprings, gens, gen, planner, toolbox):
    if isinstance(planner.parallel_pool, ParallelSimulator.ParallelSimulator):
        fit = [(list(x[1].fitness.wvalues), x[1].id if hasattr(x[1], "id") else -1) if x[1].fitness.valid else ([f*w for f,w in zip(toolbox.evaluate(x[1]), plausible_offsprings[0][1].fitness.weights)], x[1].id if hasattr(x[1], "id") else -1) for x in plausible_offsprings]
        sorted_fits = [i for (i,y) in sorted(enumerate(fit), key=lambda x: (x[1][0], x[1][1], x[0]), reverse=True)]
        maxs = 0
        mins = 0
        for i in range(number):
            if random.random() < planner.parallel_pool.good_parent_pb:
                maxs += 1
            else:
                mins += 1

        if maxs != 0 and mins != 0:
            return [plausible_offsprings[i] for i in sorted_fits[:maxs] + sorted_fits[-mins:]]
        elif maxs != 0:
            return [plausible_offsprings[i] for i in sorted_fits[:maxs]]
        elif mins != 0:
            return [plausible_offsprings[i] for i in sorted_fits[-mins:]]

    fit = [list(x[1].fitness.wvalues) if x[1].fitness.valid else [] for x in plausible_offsprings]
    counts = len([1 for i in fit if i])
    if counts >= number:
        maxs = [i for (i,y) in heapq.nlargest(number, enumerate(fit), key=lambda x: (x[1], x[0]))]
        return [plausible_offsprings[i] for i in maxs]
    else:
        maxs = [i for (i,y) in heapq.nlargest(counts, enumerate(fit), key=lambda x: (x[1], x[0]))]
        possible = [ x for x in range(len(plausible_offsprings)) if x not in maxs]
        maxs += random.sample(possible, number - counts)

        return [plausible_offsprings[i] for i in maxs]


def _do_tournament(idx, asp1, asp2, asp1_trans, asp2_trans, tournament_valid, speculative_tournament, toolbox, gens,
                   gen, planner, surrogate_model):
    speculative_tournament_copy = copy.deepcopy(speculative_tournament)

    options_1 = [(Transition(asp1_trans.source, asp1_trans.source_index, i), a) for i,a in enumerate(asp1)]
    options_2 = [(Transition(asp2_trans.source, asp2_trans.source_index, i), a) for i,a in enumerate(asp2)]
    options = options_1 + options_2
    winners = surrogate_model.estimate_tournament_winners(options=options, valid=tournament_valid, last_winners=speculative_tournament, toolbox=toolbox)
    winners_trans = [t for t,a in winners]

    wins = [] if not speculative_tournament else [None]*len(speculative_tournament)
    trans = [] if not speculative_tournament else [None]*len(speculative_tournament)

    # if the individual was in last tournament, we put it to its position, others individuals we put to the end of list
    # pokud uz jedinec byl v minulem turnaji, dame ho na jeho misto, ostatni pridavame na konec
    for a1 in range(len(asp1)):
        if options_1[a1][0].in_collection(winners_trans, Transition.Mask.SPECULATIVE_SOURCE):
            position = options_1[a1][0].in_collection(speculative_tournament_copy, Transition.Mask.SPECULATIVE_SOURCE)
            if position:
                wins[position[0].destination_list_index] = toolbox.clone(asp1[a1])
                trans[position[0].destination_list_index] = position[0]
                # if the tournament is between two same individuals, we have to remove the individual to avoid rewriting
                # pokud je turnaj mezi stejnymi jedinci, musim odebirat, abych si je neprepsal
                speculative_tournament_copy.remove(position[0])
            else:
                wins.append(toolbox.clone(asp1[a1]))
                trans.append(Transition(asp1_trans.source, asp1_trans.source_index, a1, idx, len(wins)-1))

    if True or not asp1_trans.in_collection([asp2_trans], Transition.Mask.SPECULATIVE_SOURCE):
        for a2 in range(len(asp2)):
            if options_2[a2][0].in_collection(winners_trans, Transition.Mask.SPECULATIVE_SOURCE):
                position = options_2[a2][0].in_collection(speculative_tournament_copy, Transition.Mask.SPECULATIVE_SOURCE)
                if position:
                    wins[position[0].destination_list_index] = toolbox.clone(asp2[a2])
                    trans[position[0].destination_list_index] = position[0]
                    # if the tournament is between two same individuals, we have to remove the individual to avoid rewriting
                    # pokud je turnaj mezi stejnymi jedinci, musim odebirat, abych si je neprepsal
                    speculative_tournament_copy.remove(position[0])
                else:
                    wins.append(toolbox.clone(asp2[a2]))
                    trans.append(Transition(asp2_trans.source, asp2_trans.source_index, a2, idx, len(wins) - 1))

    # Removing individuals, which stay in the invalid tournament, selected in last tournament,
    # but not in the actual tournament (positions with None)
    # we will renumber the destination indices, we need renumber new transitions too
    # odstraneni jedincu, kteri zustali v nevalidnim turnaji a pri novem turnaji vypadli (pozice s None)
    # budou se posouvat destination indexy, musim precislovat i nove trans
    removed_count = 0
    for dli, t in enumerate(trans):
        if not t:
            position = Transition(destination_index=idx, destination_list_index=dli-removed_count).in_collection(speculative_tournament, Transition.Mask.SPECULATIVE_DESTINATION)
            _igea_propagate_speculative_remove(gens, gen, TransitionType.TOURNAMENT, position[0], planner, Task.Terminated.BAD_TOURNAMENT, idx)
            removed_count += 1
            for item in trans:
                if item and item.destination_list_index > dli-removed_count:
                    item.destination_list_index -= 1
    wins = [x for x in wins if x]
    trans = [x for x in trans if x]
    return wins, trans


def _igea_propagate_speculative(ea_type, id, gens, ind_gen, toolbox, planner, mu, lambda_, cxpb, mutpb,
                                surrogate_model):
    working_gen = gens[ind_gen]

    # check existence of the next generation, if the next generation does not exist, we create it
    # kontrola existence dalsi generace, pokud neexistuje, tak ji vytvorim
    if len(gens) == ind_gen + 1:
        aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
        g = utils.Generation(mu, lambda_, tournament_aspirants=aspirant_pairs)
        g.invalid = 0
        gens.append(g)

    # check existence of the next generation of next generation, if it does not exist, we create it
    # due to CLEVER TOURNAMENT, which needs aspirants pair for the tournament
    # kontrola existence o 2 dalsi generace, pokud neexistuje, tak ji vytvorim - kvuli CLEVER TOURNAMENT, který potrebuje pary do turnaje
    if len(gens) == ind_gen+2:
        aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
        g = utils.Generation(mu, lambda_, tournament_aspirants=aspirant_pairs)
        g.invalid = 0
        gens.append(g)

    # get computed individuals and speculative individuals, which can propagate to the next generation
    # If all speculative individuals at one position are evaluated, we consider they are evaluated
    # If not all speculative individuals at one position are evaluated, we consider they are not evaluated
    # ziskani ohodnocenych jedincu a spekulativnich jedincu (pokud jich je vic, tak vsechny - pocitam jako vyhodnocena,
    # pokud jen cast jako nevyhodnocene, ale dal se mohou dostat) do dalsi generace
    next_gen = gens[ind_gen + 1]
    evaluated_parents = [(Transition(Source.PARENT, idx), par) for idx, par in enumerate(working_gen.pop) if par and par.fitness.valid]
    evaluated_offspring = [(Transition(Source.OFFSPRING, idx), off) for idx, off in enumerate(working_gen.offsprings) if
                           off and off.fitness.valid]
    evaluated_parents_speculative = [(Transition(Source.SPECULATIVE_PARENT, idx), par) for idx, par in enumerate(working_gen.speculative_pop) if
                           par and all([x.fitness.valid for x in par])]
    evaluated_offspring_speculative = [(Transition(Source.SPECULATIVE_OFFSPRING, idx), off) for idx, off in enumerate(working_gen.speculative_offsprings) if
                           off and all([x.fitness.valid for x in off])]
    semievaluated_parents_speculative = [(Transition(Source.SPECULATIVE_PARENT, idx), par) for idx, par in enumerate(working_gen.speculative_pop) if
                           par and not all([x.fitness.valid for x in par]) and any([x.fitness.valid for x in par])]
    semievaluated_offspring_speculative = [(Transition(Source.SPECULATIVE_OFFSPRING, idx), off) for idx, off in enumerate(working_gen.speculative_offsprings) if
                           off and not all([x.fitness.valid for x in off]) and any([x.fitness.valid for x in off])]
    fitnesses = [x[1] for x in evaluated_offspring] + [offs[1][np.argmax([off.fitness for off in offs[1]])] for offs in evaluated_offspring_speculative]
    if ea_type == EAType.PLUS:
        fitnesses += [x[1] for x in evaluated_parents] + [pars[1][np.argmax([par.fitness for par in pars[1]])] for pars in evaluated_parents_speculative]
    if ((ea_type != EAType.PLUS and len(fitnesses) <= lambda_ - mu)
            or (ea_type == EAType.PLUS and len(fitnesses) <= lambda_)):  # nedostatek ohodnocenych jedincu, nevime, kteri budou patrit do dalsi generace
        return

    # We choose individuals, in which we are sure, that they propagate to the next generation and they are not there yet
    # vybereme jedince, kteri urcite postoupi do dalsi generace a vybereme jen ty, ktere jsme tam jeste nepridali
    sorted_evaluated = sorted(fitnesses, key=lambda x: x.fitness)
    selected = sorted_evaluated[(lambda_ - mu):] if ea_type != EAType.PLUS else sorted_evaluated[(lambda_):]

    new_selected = []
    for inds in evaluated_offspring_speculative:
        for i, ind in enumerate(inds[1]):
            t = copy.deepcopy(inds[0])
            t.source_list_index = i
            if (selected[0].fitness <= ind.fitness
                    and not t.in_collection(next_gen.speculative_in_pop, Transition.Mask.SOURCE)):
                new_selected.append((t, ind))

    if ea_type == EAType.PLUS:
        for inds in evaluated_parents_speculative:
            for i, ind in enumerate(inds[1]):
                t = copy.deepcopy(inds[0])
                t.source_list_index = i
                if (selected[0].fitness <= ind.fitness
                        and not t.in_collection(next_gen.speculative_in_pop, Transition.Mask.SOURCE)):
                    new_selected.append((t, ind))

    for inds in semievaluated_offspring_speculative:
        for i, ind in enumerate(inds[1]):
            t = copy.deepcopy(inds[0])
            t.source_list_index = i
            if (ind.fitness.valid
                    and selected[0].fitness <= ind.fitness
                    and not t.in_collection(next_gen.speculative_in_pop, Transition.Mask.SOURCE)):
                new_selected.append((t, ind))

    if ea_type == EAType.PLUS:
        for inds in semievaluated_parents_speculative:
            for i, ind in enumerate(inds[1]):
                t = copy.deepcopy(inds[0])
                t.source_list_index = i
                if (ind.fitness.valid
                        and selected[0].fitness <= ind.fitness
                        and not t.in_collection(next_gen.speculative_in_pop, Transition.Mask.SOURCE)):
                    new_selected.append((t, ind))

    # We add selected individuals as parents to the next generation
    # pridame vybrane jedince jako rodice do dalsi generace
    for i in new_selected:
        # test, if any individuals from the same position are in the next generation
        # testovani, jestli se nekteri jedinci z jedne pozice nenachazi uz v populaci
        similar = i[0].in_collection(next_gen.speculative_in_pop, Transition.Mask.SOURCE)
        if not similar:
            if not [j for j in range(mu) if next_gen.pop[j] is None and not next_gen.speculative_pop[j]]:
                #print(_igea_propagate_speculative.__name__, "Neni volna pozice v pop a speculative_pop")
                # new individual with the same fitness like the worst individual which can step forward to the next
                # generation -> First come, first serve.
                # novy jedinec se stejnou fitness jako nejhorsi postupujici jedinec -> kdo driv prijde ten driv mele
                continue

            # There should be not problem with free position, because in clever tournament there is only one position
            # for the one individual
            # Nemel by byt problem s tim, ze nebude volna pozice, protoze na danou pozici v Clever Tournamentu muze
            # pouze vybrany jedinec
            if ea_type == EAType.CLEVER_TOURNAMENT:
                index = i[0].source_index
                if not(next_gen.pop[index] is None and not next_gen.speculative_pop[index]):
                    print("CHYBA!!! _igea_propagate_speculative - index na obsazene pozici")
            else:
                index = next(iter([j for j in range(mu) if next_gen.pop[j] is None and not next_gen.speculative_pop[j]])) # nalezeni prvniho volneho indexu TODO muze spadnou protoze tam bude novy speculative z dalsi generace
            list_index = 0
            next_gen.speculative_pop[index] = []
        else:
            index = similar[0].destination_index
            list_index = len(next_gen.speculative_pop[index])
        next_gen.speculative_pop[index].append(toolbox.clone(i[1]))
        i[0].destination_index = index
        i[0].destination_list_index = list_index
        next_gen.speculative_in_pop.add(i[0])

        _igea_propagate_speculative_cross_mutate(ea_type, id, gens, ind_gen, toolbox, planner, mu, lambda_, cxpb, mutpb, surrogate_model)


def _igea_propagate_speculative_cross_mutate(ea_type, id, gens, ind_gen, toolbox, planner, mu, lambda_, cxpb, mutpb,
                                             surrogate_model):

    working_gen = gens[ind_gen]
    next_gen = gens[ind_gen + 1]
    # we choose aspirants pairs, in which we know both individuals and the tournament has not taken place yet
    # vybereme pary, u kterych zname oba jedince a jeste neprobehl turnaj, nebo probehl, ale s mene jedinci
    relevant_aspirants = [(idx, (asp1, asp2)) for idx, (asp1, asp2) in enumerate(next_gen.tournament_aspirants) if
                          ((next_gen.pop[asp1] or next_gen.speculative_pop[asp1])
                           and (next_gen.pop[asp2] or next_gen.speculative_pop[asp2])
                           # and (not next_gen.speculative_selected[idx]
                           #      or len(next_gen.speculative_selected[idx]) <
                           #          1 if next_gen.pop[asp1] else len(next_gen.speculative_pop[asp1]) +
                           #          1 if next_gen.pop[asp2] else len(next_gen.speculative_pop[asp2]))
                           and not next_gen.selected[idx])]
    # make selection
    # provedeni selekce
    for idx, (asp1, asp2) in relevant_aspirants:
        # tournament selection - retains the original order unless it is the first tournament
        # turnajova selekce - zachovava puvodni poradi, pokud to neni prvni turnaj
        ind_asp1 = [next_gen.pop[asp1]] if next_gen.pop[asp1] else next_gen.speculative_pop[asp1]
        ind_asp2 = [next_gen.pop[asp2]] if next_gen.pop[asp2] else next_gen.speculative_pop[asp2]
        trans_asp1 = Transition(Source.PARENT if next_gen.pop[asp1] else Source.SPECULATIVE_PARENT, asp1)
        trans_asp2 = Transition(Source.PARENT if next_gen.pop[asp2] else Source.SPECULATIVE_PARENT, asp2)
        ss, st = _do_tournament(idx, ind_asp1,ind_asp2, trans_asp1,trans_asp2, next_gen.speculative_tournament_valid[idx], next_gen.speculative_tournament[idx], toolbox, gens, ind_gen+1, planner, surrogate_model)
        # test, if new individuals are added
        # test jestli pribyli novi jedinci
        if ((next_gen.speculative_tournament_valid[idx]
             and next_gen.speculative_selected[idx] and len(next_gen.speculative_selected[idx]) == len(ss))):
            continue
        next_gen.speculative_selected[idx], next_gen.speculative_tournament[idx] = ss, st
        next_gen.speculative_tournament_valid[idx] = True

        # find start and end indices of the offsprings
        # vypocet pocatecniho a koncoveho indexu potomku
        if idx % 2 == 0:
            start, stop = idx, idx + 2
        else:
            start, stop = idx - 1, idx + 1
        # treatment of odd numbers of offsprings
        # osetreni licheho poctu potomku
        odd_last = False
        if stop > lambda_:
            odd_last = True
        # we will do genetic operators, if we have both individuals and we has not done yet
        # provedeme krizeni a mutaci, pokud mame oba jedince a jeste jsme je neprovedli


        no_speculative_offsprings = 0 if not next_gen.speculative_offsprings[start] else len(next_gen.speculative_offsprings[start])
        no_selected_start = 1
        if not next_gen.selected[start]:
            if next_gen.speculative_selected[start]:
                no_selected_start = len(next_gen.speculative_selected[start])
            else:
                no_selected_start = 0
        no_selected_stop = 1
        if not next_gen.selected[stop - 1]:
            if next_gen.speculative_selected[stop - 1]:
                no_selected_stop = len(next_gen.speculative_selected[stop - 1])
            else:
                no_selected_stop = 0


        # new_selected = ((0 if not next_gen.speculative_offsprings[start] else len(next_gen.speculative_offsprings[start])) <
        #                 (1 if next_gen.selected[start] else len(next_gen.speculative_selected[start]))*
        #                 (1 if next_gen.selected[stop - 1] else len(next_gen.speculative_selected[stop - 1])))
        new_selected = no_speculative_offsprings < no_selected_start*no_selected_stop


        if ((next_gen.selected[start] or next_gen.speculative_selected[start])
                and (next_gen.selected[stop - 1] or next_gen.speculative_selected[stop - 1])
                and ((not next_gen.offsprings[start] and (not next_gen.speculative_offsprings[start] or new_selected)))
                and (odd_last or (
                        not next_gen.offsprings[stop - 1] and (not next_gen.speculative_offsprings[stop - 1] or new_selected)))):
            s_off1 = False if next_gen.selected[start] else True
            s_off2 = False if next_gen.selected[stop-1] else True

            off1 = [next_gen.selected[start]] if not s_off1 else next_gen.speculative_selected[start]
            off2 = [next_gen.selected[stop - 1]] if not s_off2 else next_gen.speculative_selected[stop - 1]

            cross1 = Transition(Source.SPECULATIVE_PARENT if s_off1 else Source.PARENT, start).in_collection(next_gen.speculative_cross, Transition.Mask.SOURCE)
            cross2 = Transition(Source.SPECULATIVE_PARENT if s_off2 else Source.PARENT, stop-1).in_collection(next_gen.speculative_cross, Transition.Mask.SOURCE)
            new_from1 = -1 if not cross1 else max((x.source_list_index for x in cross1))
            new_from2 = -1 if not cross2 else max((x.source_list_index for x in cross2))

            new_off1 = off1[new_from1+1:]
            new_off2 = off2[new_from2+1:]
            old_off1 = off1[:new_from1+1]

            new_offspring = list(itertools.chain(*itertools.chain(itertools.product(new_off1, off2),itertools.product(old_off1,new_off2))))
            # offspring = itertools.chain(*itertools.product(map(toolbox.clone, off1),map(toolbox.clone, off2)))
            # offspring = map(toolbox.clone, off1 + off2)
            offspring = algorithms.varAnd(new_offspring, toolbox, cxpb, mutpb)

            if not next_gen.speculative_offsprings[start]:
                next_gen.speculative_offsprings[start] = []
            next_gen.speculative_offsprings[start] += offspring[::2]

            trans = list(itertools.chain(itertools.product(range(new_from1+1, len(off1)),range(len(off2))),itertools.product(range(new_from1+1),range(new_from2+1,len(off2)))))
            old_trans = Transition(destination_index=start).in_collection(next_gen.speculative_cross, Transition.Mask.DESTINATION)
            last_trans = -1 if not old_trans else max(old_trans, key=lambda x: x.destination_list_index).destination_list_index
            for t1,t2 in trans:
                next_gen.speculative_cross.add(Transition(Source.SPECULATIVE_PARENT if s_off1 else Source.PARENT, start, t1, start, last_trans+1))
                next_gen.speculative_cross.add(Transition(Source.SPECULATIVE_PARENT if s_off2 else Source.PARENT, stop-1, t2, start, last_trans+1))
                last_trans+=1

            if not odd_last:
                if not next_gen.speculative_offsprings[stop - 1]:
                    next_gen.speculative_offsprings[stop - 1] = []
                next_gen.speculative_offsprings[stop - 1] += offspring[1::2]

                old_trans = Transition(destination_index=stop-1).in_collection(next_gen.speculative_cross, Transition.Mask.DESTINATION)
                last_trans = -1 if not old_trans else max(old_trans, key=lambda x: x.destination_list_index).destination_list_index
                for t1,t2 in trans:
                    next_gen.speculative_cross.add(Transition(Source.SPECULATIVE_PARENT if s_off1 else Source.PARENT, start, t1, stop-1, last_trans+1))
                    next_gen.speculative_cross.add(Transition(Source.SPECULATIVE_PARENT if s_off2 else Source.PARENT, stop-1, t2, stop-1, last_trans+1))
                    last_trans+=1

            # if necessary, we evaluate new offsprings
            # pokud je to potreba, ohodnotime nove potomky
            for pos, new_inds in enumerate(next_gen.speculative_offsprings[start:stop]):
                for i, new_ind in enumerate(new_inds):
                    new_ind.idx = start + pos
                    new_ind.gen = ind_gen + 1
                    new_ind.speculative_idx = i
                    # add id only for new offsprings
                    if len(new_inds)-len(new_offspring)/2 <= i:
                        new_ind.id = id[0]
                        id[0] += 1
                    if not new_ind.fitness.valid and (ea_type != EAType.CLEVER_TOURNAMENT or new_ind.idx in gens[ind_gen + 2].aspirants) and not list(x for x in next_gen.speculative_waiting if x[0] == new_ind.idx and x[1] == new_ind.speculative_idx):
                        t = Task.Task(new_ind.id, Task.Speculative.UNKNOWN,
                                      sum([x.count(new_ind) if x is not None else 0 for x in next_gen.speculative_pop]), (ind_gen + 1), new_ind)
                        next_gen.speculative_invalid_ind += 1
                        planner.add(t)
                        next_gen.speculative_waiting.add((new_ind.idx, new_ind.speculative_idx, t))
    _igea_propagate_speculative(ea_type, id, gens, ind_gen + 1, toolbox, planner, mu, lambda_, cxpb,
                                mutpb, surrogate_model)  # we added a new parent, propagate the info


def _igea_propagate_speculative_remove_bad(evaluated_inds, gens, gen, mu, lambda_, planner):
    bads = evaluated_inds[:-mu]

    for bad in bads:
        in_speculative = bad[0].in_collection(gens[gen].speculative, Transition.Mask.SOURCE)
        for ind in in_speculative:
            _igea_propagate_speculative_remove(gens, gen, TransitionType.SPECULATIVE, ind, planner, Task.Terminated.BAD)


def _igea_propagate_speculative_remove_selected(idx, winner, gens, gen, planner):
    if gens[gen].speculative_tournament[idx]:
        while len(gens[gen].speculative_tournament[idx]) > 1:
            t = gens[gen].speculative_tournament[idx][0]
            if t.in_collection([winner], Transition.Mask.SOURCE):
                t = gens[gen].speculative_tournament[idx][1]
            _igea_propagate_speculative_remove(gens, gen, TransitionType.TOURNAMENT, t, planner, Task.Terminated.BAD_TOURNAMENT, idx)
        # remove from speculative selected and the correct one remove too, it will be in selected, and fix cross
        # odstranit ze speculative selected i toho spravneho -> bude v selected a opravit cross
        t = gens[gen].speculative_tournament[idx][0]
        cross_items = Transition(Source.SPECULATIVE_PARENT, t.destination_index).in_collection(gens[gen].speculative_cross, Transition.Mask.SOURCE)
        for i in cross_items:
            i.source = Source.PARENT
            # i.source_list_index = None # kvuli dopocitavani pozice v speculative offspring, tam potrebudu mit cislo
        _igea_propagate_speculative_remove(gens, gen, TransitionType.TOURNAMENT, t, planner, Task.Terminated.TRANSFER, idx)
    if gens[gen].speculative_tournament[idx] and len(gens[gen].speculative_tournament[idx]) != 1:
        a=5


def _igea_propagate_speculative_remove(gens, gen, transition_table, transition, planner, termination_type,
                                       idx_tournament = None):
    a_trans = transition
    a_g = gen
    def test(x):
        if x:
            max_SI = 0
            if gens[27].speculative_offsprings[x.source_index]:
                max_SI = len(gens[27].speculative_offsprings[x.source_index])
            if max_SI <= x.source_list_index:
                return True
            else:
                return False
        else:
            return False
    if len(gens) > 28 and any(map(test, gens[28].speculative_in_pop)):
        a=5 #TODO remove - odstranit
    # the individual which we evaluated was wrong
    # task ktery jsme pocitali byl spatny - nedostane se dal
    if transition_table == TransitionType.SPECULATIVE:
        # remove from speculative
        # odstranit ze speculative
        gens[a_g].speculative.remove(a_trans)

        _igea_propagate_speculative_remove(gens, a_g, TransitionType.PARENT, a_trans, planner, termination_type)

        # renumbering input to parents - speculative - destination_list_index
        # precislovat vstupy do parents - speculative - destination_list_index
        items = a_trans.in_collection(gens[a_g].speculative, Transition.Mask.DESTINATION)
        for item in items:
            if item.destination_list_index > a_trans.destination_list_index:
                item.destination_list_index -= 1

    elif transition_table == TransitionType.PARENT:
        # remove from parents
        # odstranit z parents
        del gens[a_g].speculative_pop[a_trans.destination_index][a_trans.destination_list_index]

        valid_tournament = True
        if not gens[a_g].speculative_pop[a_trans.destination_index]:
            valid_tournament = False

        # remove all occurences in in_pop - plus version
        # odebrat vsechny vyskyty v in_pop - postup u verze plus
        in_pop = Transition(Source.SPECULATIVE_PARENT, a_trans.destination_index, a_trans.destination_list_index)
        if in_pop.in_collection(gens[a_g + 1].speculative_in_pop, Transition.Mask.SPECULATIVE_SOURCE):
            _igea_propagate_speculative_remove(gens, a_g + 1, TransitionType.IN_POP, in_pop, planner,
                                               termination_type)
        # renumber in_pop
        # precislovat in_pop
        items2 = Transition(Source.SPECULATIVE_PARENT, a_trans.destination_index,
                            a_trans.destination_list_index).in_collection(gens[a_g + 1].speculative_in_pop,
                                                                       Transition.Mask.SOURCE)
        for item2 in items2:
            if item2.source_list_index > a_trans.destination_list_index:
                item2.source_list_index -= 1

        # remove all occurencies in tournament (we store only winners)
        # odebrat vsechny vyskyty v turnajich (vyteznych, prohrane se nezapisuji)
        a_trans = Transition(Source.SPECULATIVE_PARENT, a_trans.destination_index, a_trans.destination_list_index)

        for idx, (asp1, asp2) in enumerate(gens[a_g].tournament_aspirants):
            if asp1 == a_trans.source_index or asp2 == a_trans.source_index:
                # mark tournament as invalid
                # oznacit turnaj jako nevalidni
                if not valid_tournament:
                    gens[gen].speculative_tournament_valid[idx] = False

                # test if the individual won tournament in position idx (warning: items are changing - renumbering,
                # we need load them every iteration), but items should be <= 1, because the individual can step forward only once
                # test, jestli vyhral turnaj na pozici idx (items se mění)
                # ale items by melo byt <= 1, protoze z jedne pozice v turnaji (i s destination list index) muse postoupit jen jednou
                while True:
                    items = a_trans.in_collection(gens[a_g].speculative_tournament[idx], Transition.Mask.SPECULATIVE_SOURCE)
                    if not items:
                        break

                    if len(items) > 1:
                        a=5 #TODO odstranit
                    item = items[0]
                    _igea_propagate_speculative_remove(gens, a_g, TransitionType.TOURNAMENT, item, planner, termination_type, idx)

                # renumber outputs from parents - inputs tournament
                # we have to renumber positions after while cycle. Otherwise we set another individual to the possition
                # to delete and this individual will be deleted in the next iteration of while cycle
                # precislovat vystupy z parents - vstupy tournament
                # musi byt az po while, jinak na pozici odstraneneho prevedu jine prvky a odstranim je diky while
                items2 = a_trans.in_collection(gens[a_g].speculative_tournament[idx], Transition.Mask.SOURCE)
                for item2 in items2:
                    if item2.source_list_index > a_trans.source_list_index:
                        item2.source_list_index -= 1

    # the last winner of tournament lost in actual tournament
    # do turnaje vstoupil novy hrac a stavajici vytez prohral
    elif transition_table == TransitionType.TOURNAMENT:
        # remove from speculative_tournament (transition table from parents to selected)
        # odstranit z tournaments (prechodova tabulka parents selected)
        gens[a_g].speculative_tournament[idx_tournament].remove(a_trans)
        # remove from speculative_selected
        # odstranit ze selected
        del gens[a_g].speculative_selected[idx_tournament][a_trans.destination_list_index]
        # renumber inputs to speculative_selected (tournaments - destination_list_index)
        # precislovat vstupy do selected - tournaments - destination_list_index
        items = a_trans.in_collection(gens[a_g].speculative_tournament[idx_tournament], Transition.Mask.DESTINATION)
        for item in items:
            if item.destination_list_index > a_trans.destination_list_index:
                item.destination_list_index -= 1

        # remove from cross
        # odstranit z cross
        _igea_propagate_speculative_remove(gens, a_g, TransitionType.CROSS, Transition(Source.SPECULATIVE_PARENT, a_trans.destination_index, a_trans.destination_list_index), planner, termination_type)
        # renumber inputs to cross
        # precislovat vstupy do cross
        items = Transition(Source.SPECULATIVE_PARENT, a_trans.destination_index).in_collection(gens[a_g].speculative_cross, Transition.Mask.SOURCE)
        for item in items:
            if item.source_list_index > a_trans.destination_list_index:
                item.source_list_index -= 1

    elif transition_table == TransitionType.CROSS:
        # remove from cross
        # odstranit z cross
        # while True:
        #     items = a_trans.in_collection(gens[a_g].speculative_cross, Transition.Mask.SPECULATIVE_SOURCE)
        #     if not items:
        #         break
        #
        #     if len(items) > 1:
        #         a = 5 #TODO remove - odstranit
        #     item = items[0]
        for item in a_trans.in_collection(gens[a_g].speculative_cross, Transition.Mask.SPECULATIVE_SOURCE):
            try:
                x = gens[a_g].speculative_offsprings[item.destination_index][item.destination_list_index]
            except:
                print("err")

            # remove from cross, we have to go backward to remove both parents
            # odstranit z cross, musim jit zpetne abych odstranil oba rodice
            items2 = item.in_collection(gens[a_g].speculative_cross, Transition.Mask.SPECULATIVE_DESTINATION)
            for item2 in items2:
                gens[a_g].speculative_cross.remove(item2)
            # remove from offsprings
            # odstranit z offspring
            if termination_type != Task.Terminated.TRANSFER:
                planner.log.algorithm_log(gens[a_g].speculative_offsprings[item.destination_index][item.destination_list_index].id, Task.Speculative.BAD)
            del gens[a_g].speculative_offsprings[item.destination_index][item.destination_list_index]

            # renumber outputs from cross
            # precislovat vystupy cross
            items2 = item.in_collection(gens[a_g].speculative_cross, Transition.Mask.DESTINATION)
            for item2 in items2:
                if item2.destination_list_index > item.destination_list_index:
                    item2.destination_list_index -= 1

            # remove from waitings and remove task from planner
            # odstranit z waitings a ukoncit task
            waitings = [x for x in gens[a_g].speculative_waiting if x[0] == item.destination_index and x[1] == item.destination_list_index]
            if(len(waitings) == 1):
                planner.remove(waitings[0][2], termination_type)
                gens[a_g].speculative_waiting.remove(waitings[0])
            elif(len(waitings)>1):
                # ERROR: Individual can be only once in waiting
                # chyba nemusze byt vice waitings
                raise Exception("Task se ve waitings nachází vícekrát")

            # renumber waitings
            # precislovat waitings
            remove = set()
            append = set()
            items2 = [w for w in gens[a_g].speculative_waiting if w[0] == item.destination_index and w[1] > item.destination_list_index]
            for w in items2:
                append.add((w[0], w[1]-1, w[2]))
                remove.add(w)

            remove = sorted(remove)
            append = sorted(append)

            for r in remove:
                gens[a_g].speculative_waiting.remove(r)
            for a in append:
                gens[a_g].speculative_waiting.add(a)

            if len(waitings) == 0:
                in_pop = Transition(Source.SPECULATIVE_OFFSPRING, item.destination_index, item.destination_list_index)
                if in_pop.in_collection(gens[a_g+1].speculative_in_pop, Transition.Mask.SPECULATIVE_SOURCE):
                    _igea_propagate_speculative_remove(gens, a_g + 1, TransitionType.IN_POP, in_pop, planner, termination_type)
            # renumber in_pop
            # precislovat in_pop
            items2 = Transition(Source.SPECULATIVE_OFFSPRING, item.destination_index, item.destination_list_index).in_collection(gens[a_g+1].speculative_in_pop, Transition.Mask.SOURCE)
            for item2 in items2:
                if item2.source_list_index > item.destination_list_index:
                    item2.source_list_index -= 1


    elif transition_table == TransitionType.IN_POP:

        items = a_trans.in_collection(gens[a_g].speculative_in_pop, Transition.Mask.SPECULATIVE_SOURCE)
        for item in items:
            _igea_propagate_speculative_remove(gens, a_g, TransitionType.PARENT, item, planner, termination_type)

        # remove from in_pop
        # odstranit z in_pop
        gens[a_g].speculative_in_pop.remove(items[0])

        items2 = items[0].in_collection(gens[a_g].speculative_in_pop, Transition.Mask.DESTINATION)
        for item2 in items2:
            if item2.destination_list_index > items[0].destination_list_index:
                item2.destination_list_index -= 1


def _igea_select_index_in_parent(ea_type, transition, gens, gen, mu, planner):
    in_speculative = transition.in_collection(gens[gen].speculative, Transition.Mask.SOURCE)
    if in_speculative:
        occurrences = [x.in_collection(gens[gen].speculative, Transition.Mask.DESTINATION) for x in in_speculative]
        best = np.argmin([len(x) for x in occurrences])
        # destination_index is same for all occurences at the one position
        # destination_index je u vsech vyskytu na jedne pozici stejny
        index = occurrences[best][0].destination_index
        # remove all selected individuals at the othes positions then the selected one, and remove all others
        # individuals from the selected position
        # odstraneni vsech vybranych jedincu na jine pozici nez index a odstraneni vsech ostatnich jedincu
        # na pozici index
        for o in range(len(occurrences)):
            for t in occurrences[o]:
                if (o == best and not t.in_collection([transition], Transition.Mask.SOURCE)
                        or o != best and t.in_collection([transition], Transition.Mask.SOURCE)):
                    _igea_propagate_speculative_remove(gens, gen, TransitionType.SPECULATIVE, t, planner, Task.Terminated.BAD_POSITION)
        _igea_transfer_from_speculative_pop_to_pop(transition, gens, gen, planner)
        return index
    else:
        if not [i for i in range(mu) if gens[gen].pop[i] is None and not gens[gen].speculative_pop[i]]:
            if not gens[gen].speculative:
                #print(_igea_select_index_in_parent.__name__,
                #      "V gens[gen].speculative není žádný jedinec a všechny pozice v gens[gen].pop + speculative_pop jsou obsazeny")
                # Because not all individuals who have the minimum required fitness (multiple individuals with the same
                # fitness) may fit into the speculative pop, there may be a situation that in speculative pop there
                # is an individual who interferes with the individuals stepping forward to pop (they have the same
                # fitness) We have to remove an individual from speculative_pop with the same fitness as the entering
                # individual, ie with the lowest possible fitness
                # Protoze se nemusi do speculative pop vejit vsichni jedinci, kteri maji minimalni pozadovanou fitness
                # (vice jedincu se stejnou fitness), muze nastat situace, ze v speculative pop je jedinec, ktery
                # prekazi jedinci postupujicimu do pop (maji stejnou fitness) musim tedy odstranit jedince ze
                # speculative_pop se stejnou fitness jako ma vstupujici jedinec, tedy s nejnizsi moznou fitness
                max_fitnesses = [(idx, max([y.fitness for y in x])) for idx, x in enumerate(gens[gen].speculative_pop) if x]
                argmin = min(max_fitnesses, key=lambda x: x[1])[0]
                while True:
                    in_pops = Transition(destination_index=argmin).in_collection(gens[gen].speculative_in_pop, Transition.Mask.DESTINATION) # musim smazat celou pozici
                    if not in_pops:
                        break
                    _igea_propagate_speculative_remove(gens, gen, TransitionType.IN_POP, in_pops[0], planner, Task.Terminated.CROWDED)
            else:
                sample = random.sample(sorted(gens[gen].speculative), 1)[0]
                while(True):
                    samples = sample.in_collection(gens[gen].speculative, Transition.Mask.DESTINATION)
                    if not samples:
                        break
                    _igea_propagate_speculative_remove(gens, gen, TransitionType.SPECULATIVE, samples[0], planner, Task.Terminated.CROWDED)

        if not [j for j in range(mu) if gens[gen].pop[j] is None and not gens[gen].speculative_pop[j]]:
            a = 5 # TODO remove - odstranit
        if ea_type == EAType.CLEVER_TOURNAMENT:
            index = transition.source_index
        else:
            # find first free position
            # nalezeni prvniho volneho indexu
            index = next(iter([j for j in range(mu) if gens[gen].pop[j] is None and not gens[gen].speculative_pop[j]]))
        return index


def _igea_transfer_from_speculative_pop_to_pop(transition, gens, gen, planner):
    # change the individual origin from speculative parent to parent
    # zmenit puvod jedince z speculative parent to parent
    transitions = transition.in_collection(gens[gen].speculative, Transition.Mask.SOURCE)
    assert len(transitions) == 1, "The number of occurrences of an individual must be 1, but it is %s" % (len(transitions))
    transition = transitions[0]
    parent_transition = Transition(Source.SPECULATIVE_PARENT, transition.destination_index, transition.destination_list_index)

    # plus version - move parents (in transitions) from speculative_in_pop to speculative (next generation)
    # it can happen that the actual individual steps forward to the next generation, then we have to edit the transition
    # plus verze - presun rodice (v transition) ze speculative_in_pop do speculative (dalsi generace)
    # muze se stat, ze aktualni jedinec primo postoupi do dalsi generace, pak musime upravit odkaz
    in_pop = parent_transition.in_collection(gens[gen+1].speculative_in_pop, Transition.Mask.SPECULATIVE_SOURCE)
    assert len(in_pop) <= 1, "The number of stepping forward individuals must be at most 1, but it is %s" % (len(in_pop))
    if in_pop:
        gens[gen+1].speculative.add(Transition(Source.PARENT, in_pop[0].source_index, None, in_pop[0].destination_index, in_pop[0].destination_list_index))
        gens[gen+1].speculative_in_pop.remove(in_pop[0])

    occurrences = [idx for idx, (asp1, asp2) in enumerate(gens[gen].tournament_aspirants) if asp1 == parent_transition.source_index or asp2 == parent_transition.source_index]
    for o in occurrences:
        for t in parent_transition.in_collection(gens[gen].speculative_tournament[o], Transition.Mask.SPECULATIVE_SOURCE):
            t.source = Source.PARENT
    # remove from speculative and speculative_pop
    # odstranit ze speculative a speculative_pop
    _igea_propagate_speculative_remove(gens, gen, TransitionType.SPECULATIVE, transition, planner, Task.Terminated.TRANSFER)


def _igea_transfer_offsprings(ea_type, start_idx, stop_idx, id, gens, gen, planner, toolbox, lambda_, cxpb, mutpb):
    start_cross = Transition(None, start_idx).in_collection(gens[gen].speculative_cross,
                                                            Transition(False, True, False, False, False))
    stop_cross = Transition(None, stop_idx).in_collection(gens[gen].speculative_cross,
                                                          Transition(False, True, False, False, False))

    # debug tests - it is wrong when number of offsprings is odd, then 1 occurence for 2 individuals at the end
    #tests - chybne pri lichem poctu offsprings, pak se musi vyskytovat jeden vyskyt na 2 na koncove pozici
    #TODO fix - opravit
    if len(start_cross) > 2 or len(start_cross) % 2 != 0:
        a=5 #TODO  remove - odstranit
    if len(stop_cross) > 2 or len(stop_cross) % 2 != 0:
        a=5 #TODO remove - odstranit
    if any(map(lambda x: x.source != Source.PARENT, start_cross + stop_cross)): # any([]) == False
        a=5 #TODO remove - odstranit
    if [x.destination_index for x in start_cross] != [x.destination_index for x in stop_cross]:
        a=5 #TODO remove - odstranit
    if any(map(lambda x: x.destination_list_index != 0, start_cross + stop_cross)):
        a=5 #TODO remove - odstranit


    if start_cross and stop_cross:
        if any(map(lambda x: len(gens[gen].speculative_offsprings[x.destination_index]) > 1, start_cross)):
            a = 5  # TODO remove - odstranit

        for cross in start_cross:
            #move to the offsprings
            # presun do offsprings
            planner.log.algorithm_log(gens[gen].speculative_offsprings[cross.destination_index][cross.destination_list_index].id, Task.Speculative.GOOD)
            gens[gen].offsprings[cross.destination_index] = toolbox.clone(gens[gen].speculative_offsprings[cross.destination_index][cross.destination_list_index])
            # move to the waitings
            # presun do waitings
            waitings = [x for x in gens[gen].speculative_waiting if x[0] == cross.destination_index and x[1] == cross.destination_list_index]
            assert len(waitings) <= 1, "Nemůžu čekat na více výsledků, jednoho jedince"
            if waitings:
                gens[gen].waiting.add(cross.destination_index)
                waitings[0][2].speculative = Task.Speculative.GOOD
                gens[gen].speculative_waiting.remove(waitings[0])
            else:
                # task is evaluated, we have to edit number of good tasks
                # task je dopocitan musim upravit pocty dobrych tasku
                planner.finished_good_tasks += 1
                planner.finished_not_good_tasks -= 1

                in_pop = Transition(Source.SPECULATIVE_OFFSPRING, cross.destination_index, cross.destination_list_index)
                remove = set()
                for x in gens[gen+1].speculative_in_pop:
                    if in_pop.in_collection([x], Transition.Mask.SPECULATIVE_SOURCE):
                        # speculative = Transition(Source.OFFSPRING, x.source_index, x.source_list_index, x.destination_index, x.destination_list_index)
                        speculative = Transition(Source.OFFSPRING, x.source_index, None, x.destination_index, 0)
                        remove.add(x)
                        gens[gen+1].speculative.add(speculative)
                for x in remove:
                    gens[gen + 1].speculative_in_pop.remove(x)

    # We will do genetic operators, because we don't have good result in the speculative part
    # provedeni klasickeho krizeni a mutace, protoze spekulativni nedal pozadovane vysledky
    else:
        odd_last = False
        if stop_idx == lambda_:
            odd_last = True
        offspring = map(toolbox.clone, gens[gen].selected[start_idx:stop_idx+1])
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
        if odd_last:
            gens[gen].offsprings[start_idx] = offspring[0]
        else:
            gens[gen].offsprings[start_idx:stop_idx+1] = offspring
        # if it is need, we evaluate the offsprings
        # pokud je to potreba, ohodnotime nove potomky
        for pos, new_ind in enumerate(gens[gen].offsprings[start_idx:stop_idx+1]):
            new_ind.idx = start_idx + pos
            new_ind.gen = gen
            # the individual is always new and definitive, it will never change
            # jedinec je vzdy novy a definitivni, uz se nikdy nezmeni
            new_ind.id = id[0]
            if not new_ind.fitness.valid and (ea_type != EAType.CLEVER_TOURNAMENT or new_ind.idx in gens[gen+1].aspirants):
                t = Task.Task(new_ind.id, Task.Speculative.NONE, gens[gen].pop.count(new_ind), gen, new_ind)
                gens[gen].invalid_ind += 1
                planner.add(t)
                gens[gen].waiting.add(new_ind.idx)
            id[0] += 1

    # remove from cross
    # odstranit z cross
    for c_source_idx in [start_idx, stop_idx]:
        while(True):
            crosses = Transition(None, c_source_idx).in_collection(gens[gen].speculative_cross,
                                                      Transition(False, True, False, False, False))
            if not crosses:
                break
            _igea_propagate_speculative_remove(gens, gen, TransitionType.CROSS, crosses[0], planner, Task.Terminated.TRANSFER, None)
    a = 5


def _igea_propagate(ea_type, id, gens, ind_gen, toolbox, planner, mu, lambda_, cxpb, mutpb):
    working_gen = gens[ind_gen]

    # check exitence of the next generation, if it does not exist, we create the one
    # kontrola existence dalsi generace, pokud neexistuje, tak ji vytvorim
    if len(gens) == ind_gen+1:
        aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
        g = utils.Generation(mu, lambda_, tournament_aspirants=aspirant_pairs)
        g.invalid = 0
        gens.append(g)

    # check existence of the next generation of the next generation, if it does not exist, we create the one
    # due to Clevel Tournament, which needs aspirant pairs from this generation
    # kontrola existence o 2 dalsi generace, pokud neexistuje, tak ji vytvorim - kvuli CLEVER TOURNAMENT, který
    # potrebuje pary do turnaje
    if len(gens) == ind_gen+2:
        aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
        g = utils.Generation(mu, lambda_, tournament_aspirants=aspirant_pairs)
        g.invalid = 0
        gens.append(g)

    # get evaluated individuals which will step forward to the next generation
    # ziskani ohodnocenych jedincu do dalsi generace
    next_gen = gens[ind_gen+1]
    evaluated_parents = [(Transition(Source.PARENT, idx), par) for idx, par in enumerate(working_gen.pop) if par and par.fitness.valid]
    evaluated_offspring = [(Transition(Source.OFFSPRING, idx), off) for idx, off in enumerate(working_gen.offsprings) if
                           off and off.fitness.valid]
    evaluated = evaluated_offspring
    if ea_type == EAType.PLUS:
        evaluated += evaluated_parents
    if ((ea_type == EAType.COMMA and len(evaluated) < lambda_ - mu)
            or (ea_type == EAType.PLUS and len(evaluated) < lambda_)):
        # We do not have enought evaluated individuals, so we do not know which of them will step forward
        # to the next generation
        # nedostatek ohodnocenych jedincu, nevime, kteri budou patrit do dalsi generace
        return

    # we select the individuals which will certainly step forward to the next generation and which are not there yet
    # vybereme jedince, kteri urcite postoupi do dalsi generace a vybereme jen ty, ktere jsme tam jeste nepridali
    sorted_evaluated = sorted(evaluated, key=lambda x: (x[1].fitness, x[0].source_index))

    # remove wrong speculative individuals
    # odstraneni spatnych spekulativnich tasku

    selected = sorted_evaluated[(lambda_-mu):] if ea_type != EAType.PLUS else sorted_evaluated[(lambda_):]
    new_selected = [ind for ind in selected if
                    not ind[0].in_collection(next_gen.in_pop, Transition.Mask.SOURCE)]

    # we add selected individuals as parents to the next generation
    # pridame vybrane jedince jako rodice do dalsi generace
    for i in new_selected:
        _igea_propagate_speculative_remove_bad(sorted_evaluated, gens, ind_gen + 1, mu, lambda_, planner)

        index = _igea_select_index_in_parent(ea_type, i[0], gens, ind_gen + 1, mu, planner)
        next_gen.pop[index] = toolbox.clone(i[1])
        next_gen.in_pop.add(Transition(i[0].source, i[0].source_index, destination_index=index))

        # we chose aspirant pairs in which we know both individuals and we do not make tournament yet
        # vybereme pary, u kterych zname oba jedince a jeste neprobehl turnaj
        relevant_aspirants = [(idx, (asp1, asp2)) for idx, (asp1, asp2) in enumerate(next_gen.tournament_aspirants) if next_gen.pop[asp1] and next_gen.pop[asp2] and not next_gen.selected[idx]]
        # make selection
        # provedeni selekce
        for idx, (asp1, asp2) in relevant_aspirants:
            # tournament selection
            # turnajova selekce
            next_gen.selected[idx] = max([next_gen.pop[asp1], next_gen.pop[asp2]], key=lambda x: x.fitness)
            winner = asp1 if next_gen.pop[asp1].fitness >=  next_gen.pop[asp2].fitness else asp2
            _igea_propagate_speculative_remove_selected(idx, Transition(Source.PARENT, winner), gens, ind_gen + 1, planner)
            if next_gen.speculative_selected[idx]:
                if len(next_gen.speculative_selected[idx])>1:
                    # if any([x.source == Source.SPECULATIVE_PARENT for x in next_gen.speculative_tournament[idx]]):
                        a = 5
            # compute start and end index of offsprings
            # vypocet pocatecniho a koncoveho indexu potomku
            if idx % 2 == 0:
                start, stop = idx, idx + 2
            else:
                start, stop = idx - 1, idx + 1
            # treatment of odd number of offspring
            # osetreni licheho poctu potomku
            odd_last = False
            if stop > lambda_:
                odd_last = True
            # we do genetic operators if we know both individuals and we do not make it yet
            # provedeme krizeni a mutaci, pokud mame oba jedince a jeste jsme je neprovedli
            if next_gen.selected[start] is not None and next_gen.selected[stop - 1] is not None and next_gen.offsprings[start] is None and (odd_last or next_gen.offsprings[stop - 1] is None):
                _igea_transfer_offsprings(ea_type, start, stop - 1, id, gens, ind_gen + 1, planner, toolbox, lambda_, cxpb, mutpb)
                # offspring = map(toolbox.clone, next_gen.selected[start:stop])
                # offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)
                # if odd_last:
                #     next_gen.offsprings[start] = offspring[0]
                # else:
                #     next_gen.offsprings[start:stop] = offspring
                # # pokud je to potreba, ohodnotime nove potomky
                # for pos, new_ind in enumerate(next_gen.offsprings[start:stop]):
                #     new_ind.idx = start + pos
                #     new_ind.gen = ind_gen + 1
                #     if not new_ind.fitness.valid:
                #         t = Task.Task(id[0], Task.Speculative.NONE, next_gen.pop.count(new_ind), (ind_gen+1), new_ind)
                #         id[0] += 1
                #         next_gen.invalid_ind += 1
                #         planner.add(t)
                #         next_gen.waiting.add(new_ind.idx)

        # DUMP
        # dump_log(gens, None, planner)
        # global step
        # global sub_step
        _igea_propagate(ea_type, id, gens, ind_gen + 1, toolbox, planner, mu, lambda_, cxpb, mutpb) # we added a new parent, propagate the info


#endregion


########################################################################################################################
##                                                                                                                    ##
##                                                  (mu,lambda) IGEA                                                  ##
##                                                                                                                    ##
########################################################################################################################
def igea_comma(population, toolbox, planner, mu, lambda_, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0, stats=None,
             halloffame=None, surrogate_model=None, verbose=__debug__):
    igea(EAType.COMMA, population, toolbox, planner, mu, lambda_, cxpb, mutpb, max_gen, max_evals, stats, halloffame,
         surrogate_model, verbose)


########################################################################################################################
##                                                                                                                    ##
##                                                  (mu+lambda) IGEA                                                  ##
##                                                                                                                    ##
########################################################################################################################
def igea_plus(population, toolbox, planner, mu, lambda_, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0, stats=None,
             halloffame=None, surrogate_model=None, verbose=__debug__):
    igea(EAType.PLUS, population, toolbox, planner, mu, lambda_, cxpb, mutpb, max_gen, max_evals, stats, halloffame,
         surrogate_model, verbose)


########################################################################################################################
##                                                                                                                    ##
##                                          (mu,mu) IGEA - CLEVER TOURNAMENT                                          ##
##                                                                                                                    ##
########################################################################################################################
def igea_clever_tournament(population, toolbox, planner, pop_size, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0,
                           stats=None, halloffame=None, surrogate_model=None, verbose=__debug__):
    igea(EAType.CLEVER_TOURNAMENT, population, toolbox, planner, pop_size, pop_size, cxpb, mutpb, max_gen, max_evals,
         stats, halloffame, surrogate_model, verbose)

########################################################################################################################
##                                                                                                                    ##
##                                           (mu,mu) EA - CLEVER TOURNAMENT                                           ##
##                                                                                                                    ##
########################################################################################################################
def ea_clever_tournament(population, toolbox, planner, pop_size, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0,
                         stats=None, halloffame=None, verbose=__debug__):

    assert max_gen or max_evals, "At least one of the max_gen or max_evals must be set!"

    args = {}
    args[const.MU] = pop_size
    args[const.CXPB] = cxpb
    args[const.MUTPB] = mutpb
    args[const.MAX_GEN] = max_gen
    args[const.MAX_EVALS] = max_evals
    planner.log = Logging.StatsLog(name_postfix="_planner",
                                   weights=population[0].fitness.weights if population else "[]",
                                   algorithm="clever_tournament",
                                   planner=planner,
                                   settings=args)

    id = 0
    # occurrences = Counter(population)

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    aspirant_pairs = utils.generate_aspirant_pairs(pop_size, pop_size)
    gens = [utils.Generation(pop_size, pop_size, aspirant_pairs, list(map(toolbox.clone, population)))]

    g = 0
    invalid = 0
    for idx, ind in enumerate(gens[g].pop):
        if idx in gens[g].aspirants and not ind.fitness.valid:
            ind.idx = idx
            t = Task.Task(id, Task.Speculative.NONE, gens[g].pop.count(ind), g, ind)
            id += 1
            invalid += 1
            planner.add(t)

    while not planner.all_evaluations_finished():
        t = planner.next_finished()
        gens[g].pop[t.individual.idx].fitness.values = t.value

    # Update the hall of fame with the generated individuals
    if halloffame is not None:
        halloffame.update(gens[g].pop)

    # Append the current generation statistics to the logbook
    record = stats.compile(gens[g].pop) if stats else {}
    logbook.record(gen=g, nevals=invalid, **record)
    if verbose:
        print(logbook.stream)

    while (not max_evals or planner.evals() < max_evals) and (not max_gen or g < max_gen):

        g += 1
        invalid = 0
        gens.append(utils.Generation(pop_size, pop_size,
                                     tournament_aspirants=utils.generate_aspirant_pairs(pop_size, pop_size)))

        gens[g - 1].selected = [max(gens[g - 1].pop[asp1], gens[g - 1].pop[asp2], key=lambda x: x.fitness)
                                for asp1, asp2 in gens[g - 1].tournament_aspirants]

        gens[g - 1].offsprings = map(toolbox.clone, gens[g - 1].selected)
        gens[g - 1].offsprings = algorithms.varAnd(gens[g - 1].offsprings, toolbox, cxpb, mutpb)
        # treatment of odd number of offspring
        if len(gens[g - 1].offsprings) > pop_size:
            del gens[g - 1].offsprings[-1]

        for idx, ind in enumerate(gens[g - 1].offsprings):
            if idx in gens[g].aspirants and not ind.fitness.valid:
                ind.idx = idx
                t = Task.Task(id, Task.Speculative.NONE, gens[g - 1].pop.count(ind), g, ind)
                id += 1
                invalid += 1
                planner.add(t)

        while not planner.all_evaluations_finished():
            t = planner.next_finished()
            gens[g - 1].offsprings[t.individual.idx].fitness.values = t.value

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(gens[g - 1].offsprings)

        # we want mu individuals with max fitness -> [-mu:]
        # chci mu jedinců s maximální fitness -> [-mu:]
        next_pop = sorted(gens[g - 1].offsprings, key=lambda x: x.fitness)[-pop_size:]

        gens[g].pop=next_pop[:]

        # Append the current generation statistics to the logbook
        record = stats.compile(gens[g].aspirants) if stats else {}
        logbook.record(gen=g, nevals=invalid, **record)
        if verbose:
            print(logbook.stream)

    for task in planner.running_tasks:
        planner.log.planner_log_end(task, planner.parallel_pool.time())

    return gens[-1].pop, logbook

########################################################################################################################
##                                                                                                                    ##
##                                                   (mu,lambda) EA                                                   ##
##                                                                                                                    ##
########################################################################################################################
def ea_comma(population, toolbox, planner, mu, lambda_, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0, stats=None,
             halloffame=None, verbose=__debug__):

    assert max_gen or max_evals, "At least one of the max_gen or max_evals must be set!"
    assert lambda_ >= mu, "lambda must be greater or equal to mu."

    args = {}
    args[const.MU] = mu
    args[const.LAMBDA] = lambda_
    args[const.CXPB] = cxpb
    args[const.MUTPB] = mutpb
    args[const.MAX_GEN] = max_gen
    args[const.MAX_EVALS] = max_evals
    planner.log = Logging.StatsLog(name_postfix="_planner",
                                   weights=population[0].fitness.weights if population else "[]",
                                   algorithm="ea_comma",
                                   planner=planner,
                                   settings=args)

    id = 0
    # occurrences = Counter(population)

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
    gens = [utils.Generation(mu, lambda_, aspirant_pairs, list(map(toolbox.clone, population)))]

    g = 0
    invalid = 0
    for idx, ind in enumerate(gens[g].pop):
        if not ind.fitness.valid:
            ind.idx = idx
            t = Task.Task(id, Task.Speculative.NONE, gens[g].pop.count(ind), g, ind)
            id += 1
            invalid += 1
            planner.add(t)

    while not planner.all_evaluations_finished():
        t = planner.next_finished()
        gens[g].pop[t.individual.idx].fitness.values = t.value

    # Update the hall of fame with the generated individuals
    if halloffame is not None:
        halloffame.update(gens[g].pop)

    # Append the current generation statistics to the logbook
    record = stats.compile(gens[g].pop) if stats else {}
    logbook.record(gen=g, nevals=invalid, **record)
    if verbose:
        print(logbook.stream)

    while (not max_evals or planner.evals() < max_evals) and (not max_gen or g < max_gen):

        g += 1
        invalid = 0

        gens.append(utils.Generation(mu, lambda_, tournament_aspirants=utils.generate_aspirant_pairs(mu, lambda_)))

        gens[g-1].selected = [max(gens[g-1].pop[asp1], gens[g-1].pop[asp2], key=lambda x: x.fitness)
                              for asp1, asp2 in gens[g-1].tournament_aspirants]

        gens[g-1].offsprings = map(toolbox.clone, gens[g-1].selected)
        gens[g-1].offsprings = algorithms.varAnd(gens[g-1].offsprings, toolbox, cxpb, mutpb)
        # treatment of odd number of offspring
        if len(gens[g-1].offsprings) > lambda_:
            del gens[g-1].offsprings[-1]

        for idx, ind in enumerate(gens[g-1].offsprings):
            if not ind.fitness.valid:
                ind.idx = idx
                t = Task.Task(id, Task.Speculative.NONE, gens[g-1].pop.count(ind), g, ind)
                id += 1
                invalid += 1
                planner.add(t)

        while not planner.all_evaluations_finished():
            t = planner.next_finished()
            gens[g-1].offsprings[t.individual.idx].fitness.values = t.value

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(gens[g-1].offsprings)

        # we want mu individuals with max fitness -> [-mu:]
        # chci mu jedinců s maximální fitness -> [-mu:]
        next_pop = sorted(gens[g-1].offsprings, key=lambda x: x.fitness)[-mu:]

        gens[g].pop = next_pop[:]

        # Append the current generation statistics to the logbook
        record = stats.compile(gens[g].aspirants) if stats else {}
        logbook.record(gen=g, nevals=invalid, **record)
        if verbose:
            print(logbook.stream)

    for task in planner.running_tasks:
        planner.log.planner_log_end(task, planner.parallel_pool.time())

    return gens[-1].pop, logbook


########################################################################################################################
##                                                                                                                    ##
##                                                   (mu+lambda) EA                                                   ##
##                                                                                                                    ##
########################################################################################################################
def ea_plus(population, toolbox, planner, mu, lambda_, cxpb=0.8, mutpb=0.1, max_gen=0, max_evals=0, stats=None,
            halloffame=None, verbose=__debug__):

    assert max_gen or max_evals, "At least one of the max_gen or max_evals must be set!"

    args = {}
    args[const.MU] = mu
    args[const.LAMBDA] = lambda_
    args[const.CXPB] = cxpb
    args[const.MUTPB] = mutpb
    args[const.MAX_GEN] = max_gen
    args[const.MAX_EVALS] = max_evals
    planner.log = Logging.StatsLog(name_postfix="_planner",
                                   weights=population[0].fitness.weights if population else "[]",
                                   algorithm="ea_plus",
                                   planner=planner,
                                   settings=args)

    id = 0
    # occurrences = Counter(population)

    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    aspirant_pairs = utils.generate_aspirant_pairs(mu, lambda_)
    gens = [utils.Generation(mu, lambda_, aspirant_pairs, list(map(toolbox.clone, population)))]

    g = 0
    invalid = 0
    for idx, ind in enumerate(gens[g].pop):
        if not ind.fitness.valid:
            ind.idx = idx
            t = Task.Task(id, Task.Speculative.NONE, gens[g].pop.count(ind), g, ind)
            id += 1
            invalid += 1
            planner.add(t)

    while not planner.all_evaluations_finished():
        t = planner.next_finished()
        gens[g].pop[t.individual.idx].fitness.values = t.value

    # Update the hall of fame with the generated individuals
    if halloffame is not None:
        halloffame.update(gens[g].pop)

    # Append the current generation statistics to the logbook
    record = stats.compile(gens[g].pop) if stats else {}
    logbook.record(gen=g, nevals=invalid, **record)
    if verbose:
        print(logbook.stream)

    while (not max_evals or planner.evals() < max_evals) and (not max_gen or g < max_gen):
        g += 1
        invalid = 0

        gens.append(utils.Generation(mu, lambda_, tournament_aspirants=utils.generate_aspirant_pairs(mu, lambda_)))

        gens[g-1].selected = [max(gens[g-1].pop[asp1], gens[g-1].pop[asp2], key=lambda x: x.fitness)
                              for asp1, asp2 in gens[g-1].tournament_aspirants]

        gens[g-1].offsprings = map(toolbox.clone, gens[g-1].selected)
        gens[g-1].offsprings = algorithms.varAnd(gens[g-1].offsprings, toolbox, cxpb, mutpb)
        # treatment of odd number of offspring
        if len(gens[g-1].offsprings) > lambda_:
            del gens[g-1].offsprings[-1]

        for idx, ind in enumerate(gens[g-1].offsprings):
            if not ind.fitness.valid:
                ind.idx = idx
                t = Task.Task(id, Task.Speculative.NONE, gens[g-1].pop.count(ind), g, ind)
                id += 1
                invalid += 1
                planner.add(t)

        while not planner.all_evaluations_finished():
            t = planner.next_finished()
            gens[g-1].offsprings[t.individual.idx].fitness.values = t.value

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(gens[g-1].offsprings)

        # we want mu individuals with max fitness -> [-mu:]
        # chci mu jedinců s maximální fitness -> [-mu:]
        next_pop = sorted(gens[g-1].offsprings + gens[g-1].pop, key=lambda x: x.fitness)[-mu:]

        gens[g].pop = next_pop[:]

        # Append the current generation statistics to the logbook
        record = stats.compile(gens[g].aspirants) if stats else {}
        logbook.record(gen=g, nevals=invalid, **record)
        if verbose:
            print(logbook.stream)

    for task in planner.running_tasks:
        planner.log.planner_log_end(task, planner.parallel_pool.time())

    return gens[-1].pop, logbook


########################################################################################################################
##                                                                                                                    ##
##                                                       AsyncEA                                                      ##
##                                                                                                                    ##
########################################################################################################################
def async_ea(population, toolbox, planner, pop_size, cxpb=0.8, mutpb=0.1, max_evals=0, stats=None, halloffame=None,
             verbose=__debug__):
    args = {}
    args[const.MU] = pop_size
    args[const.CXPB] = cxpb
    args[const.MUTPB] = mutpb
    args[const.MAX_EVALS] = max_evals
    planner.log = Logging.StatsLog(name_postfix="_planner",
                                   weights=population[0].fitness.weights if population else "[]",
                                   algorithm="async_ea",
                                   planner=planner,
                                   settings=args)

    MIN_POP_SIZE = pop_size//2
    pop = map(toolbox.clone, population)
    id = 0
    g = 0

    logbook = tools.Logbook()
    logbook.header = ['pseudo_gen', 'nevals'] + (stats.fields if stats else [])

    invalid = 0
    for idx, ind in enumerate(pop):
        if not ind.fitness.valid:
            ind.idx = idx
            t = Task.Task(id, Task.Speculative.NONE, 1, g, ind)
            id += 1
            invalid += 1
            planner.add(t)

    # Append the current generation statistics to the logbook
    record = stats.compile(population) if stats else {}
    logbook.record(pseudo_gen=g, nevals=invalid, **record)
    if verbose:
        print(logbook.stream)

    pop = []
    invalid = 0
    all = 0
    g += 1
    while planner.evals() < max_evals:
        result = planner.next_finished()

        result.individual.fitness.values = result.value
        ind = result.individual

        pop.append(ind)

        if halloffame is not None:
            halloffame.update(pop)

        if len(pop) < MIN_POP_SIZE:
            continue

        if len(pop) > pop_size:
            pop = list(sorted(pop, key=lambda x: x.fitness))[1:]

        valid = True
        offspring = None
        while valid:
            offspring = map(toolbox.clone, toolbox.select(pop, 2))
            offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)[0]
            valid = offspring.fitness.valid
            if valid:
                pop.append(offspring)
                all += 1
                if len(pop) > pop_size:
                    pop = list(sorted(pop, key=lambda x: x.fitness))[1:]

                if all == pop_size:
                    # Append the current generation statistics to the logbook
                    record = stats.compile(pop) if stats else {}
                    logbook.record(pseudo_gen=g, nevals=invalid, **record)
                    if verbose:
                        print(logbook.stream)
                    invalid = 0
                    all = 0
                    g += 1

        planner.add(Task.Task(id, Task.Speculative.NONE, 1, g, offspring))
        id += 1
        all += 1
        invalid += 1

        if all == pop_size:
            # Append the current generation statistics to the logbook
            record = stats.compile(pop) if stats else {}
            logbook.record(pseudo_gen=g, nevals=invalid, **record)
            if verbose:
                print(logbook.stream)
            invalid = 0
            all = 0
            g += 1

    for task in planner.running_tasks:
        planner.log.planner_log_end(task, planner.parallel_pool.time())

    return pop, logbook
