ID = "id"
START_TIME = "start_time"
END_TIME = "end_time"
TERMINATE_TIME = "terminate_time"
N_CPUS = "n_cpus"
CPU = "cpu"
VALUE = "value"
SPECULATIVE = "speculative"
TERMINATED = "terminated"
GENERATION = "generation"
INDIVIDUAL = "individual"
TIME = "time"
WEIGHTS = "weights"
FREE_CPU = "free_cpu"
ALGORITHM = "algorithm"
ENVIRONMENT = "environment"
CXPB = "cxpb"
MUTPB = "mutpb"
MU = "mu"
LAMBDA = "lambda"
MAX_GEN = "max_gen"
MAX_EVALS = "max_evals"
GOOD_PARENT_PB = "good_parent_pb"
GOOD_TOURNAMENT_PB = "good_tournament_pb"
PLANNER = "planner"
USE_ESTIMATE = "use_estimate"
ABORTABLE = "abortable"
BEST_FITNESS = "best_fitness"
SURROGATE_MODEL = "surrogate_model"


MIN_TIME = -1

COUNT_TYPE_OF_INDIVIDUAL = 9
LIST_TYPE_OF_INDIVIDUAL = ["computing_gens",
                           "computing_good_speculative_gens",
                           "computing_bad_speculative_gens",
                           "computing_unknown_speculative_gens",
                           "computing_terminated_gens",
                           "computing_terminated_bad_gens",
                           "computing_terminated_crowded_gens",
                           "computing_terminated_bad_position_gens",
                           "computing_terminated_bad_tournament_gens"]

NAMES_TYPE_OF_INDIVIDUAL = ["Regular",
                           "Speculative - good",
                           "Speculative - bad",
                           "Speculative - unknown",
                           "Terminated - new regular",
                           "Terminated - bad task",
                           "Terminated - need save regular",
                           "Terminated - bad position of task",
                           "Terminated - bad tournament (lazy delete)"]

ALG_RENAME_MAP = {
    "async_ea": "({mu}) AsyncEA",
    "ea_comma": "({mu},{lambda_}) EA",
    "igea_comma_False_NO": "({mu},{lambda_}) IGEA",
    "igea_comma_True_NO": "({mu},{lambda_}) IGEA-SE-N",
    "igea_comma_True_ONLY_BAD_TASKS": "({mu},{lambda_}) IGEA-SE-B",
    "igea_comma_True_YES": "({mu},{lambda_}) IGEA-SE-A",
    "ea_plus": "({mu}+{lambda_}) EA",
    "igea_plus_False_NO": "({mu}+{lambda_}) IGEA",
    "igea_plus_True_NO": "({mu}+{lambda_}) IGEA-SE-N",
    "igea_plus_True_ONLY_BAD_TASKS": "({mu}+{lambda_}) IGEA-SE-B",
    "igea_plus_True_YES": "({mu}+{lambda_}) IGEA-SE-A",
    "ea_clever_tournament": "({mu},{mu}) EA",
    "igea_clever_tournament_False_NO": "({mu},{mu}) IGEA",
    "igea_clever_tournament_True_NO": "({mu},{mu}) IGEA-SE-N",
    "igea_clever_tournament_True_ONLY_BAD_TASKS": "({mu},{mu}) IGEA-SE-B",
    "igea_clever_tournament_True_YES": "({mu},{mu}) IGEA-SE-A",
}