import random
from abc import ABC, abstractmethod

class SurrogateModel(ABC):
    @abstractmethod
    def estimate_next_parents(self, options, toolbox, num=None):
        pass

    @abstractmethod
    def estimate_tournament_winners(self, options, toolbox, valid, last_winners):
        pass

    @abstractmethod
    def fit(self, tasks):
        pass

    @abstractmethod
    def get_settings(self):
        pass




class SurrogateModelSimulation(SurrogateModel):
    def __init__(self, probability, num_tournamnet_winners):
        self.probability = probability
        self.num_tournamnet_winners = num_tournamnet_winners

    def _estimate_sort(self, options, toolbox):
        fs = [list(x.fitness.wvalues) if x.fitness.valid else [f * w for f, w in zip(toolbox.evaluate(x), x.fitness.weights)] for t,x in options]
        argsort = list([idx for idx, y in sorted(enumerate(fs), key=lambda x: x[1], reverse=True)])

        options_sorted = []
        while len(argsort) != 0:
            idx = 0
            while True:
                if idx == len(argsort) - 1:
                    options_sorted.append(options[argsort[idx]])
                    del argsort[idx]
                    break
                if random.random() <= self.probability:
                    options_sorted.append(options[argsort[idx]])
                    del argsort[idx]
                    break
                else:
                    idx += 1

        return options_sorted

    def estimate_next_parents(self, options, toolbox, num=None):
        if num == None:
            num = len(options)

        options_sorted = self._estimate_sort(options, toolbox)

        return options_sorted[:num]

    def estimate_tournament_winners(self, options, toolbox, valid, last_winners):
        fs_valid = [x.fitness.valid for t, x in options]

        if all(fs_valid):
            fs = [list(x.fitness.wvalues) for t, x in options]
            argsort = list([idx for idx, y in sorted(enumerate(fs), key=lambda x: x[1], reverse=True)])
            options_sorted = [options[arg_idx] for arg_idx in argsort]

        else:
            options_sorted = self._estimate_sort(options, toolbox)

        return options_sorted[:self.num_tournamnet_winners]

    def fit(self, tasks):
        pass

    def get_settings(self):
        return {"Name": self.__class__.__name__,
                "probability": self.probability,
                "num_tournamnet_winners": self.num_tournamnet_winners
                }