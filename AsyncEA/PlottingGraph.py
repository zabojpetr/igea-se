import os
import json
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import colors
import datetime
from itertools import chain, cycle
import numpy as np
import pandas as pd
import math

from AsyncEA import const
from AsyncEA import utils

import datetime

class PlottingGraph:
    def __init__(self, *args):
        self.summaries = args
        self.filename_prefixes = [""]*len(self.summaries)
        self.filename_prefix = ""

    @classmethod
    def from_summary_file(cls, *args):
        if not args:
            return None
        summaries = []
        filename_prefixes = []
        for filename in args:
            try:
                file = open(filename, "r")
                summaries.append(json.load(file))
                filename_prefixes.append(os.path.splitext(filename)[0])
                file.close()
            except FileNotFoundError:
                print(f"File {filename} not found!")
                return None
            except:
                print(f"File {filename} cannot parse!")
                return None

        c = cls(*summaries)
        c.filename_prefixes = filename_prefixes
        c.filename_prefix = os.path.commonprefix(filename_prefixes)
        return c


    def cpu_used_graph(self, output_file_names=None, created_files=True, use_summaries=None):
        """
        Create graphs of number of cpu usage by generations.
        :param output_file_names: array of names, if one name then it is prefix
        :param created_file: Create image files
        :param use_summaries: array of numbers of summaries which are use
        :return:
        """
        postfix = "_CpuUsed.pdf"
        if use_summaries == None:
            use_summaries = list(range(len(self.summaries)))
        if output_file_names == None:
            if not "" in self.filename_prefixes:
                output_file_names = [self.filename_prefixes[i] + postfix for i in use_summaries]
            elif "" != self.filename_prefix:
                output_file_names = [self.filename_prefix + "_" + i + postfix for i in use_summaries]
            else:
                self.filename_prefix = utils.create_log_name(datetime.datetime.now())
                output_file_names = [self.filename_prefix + "_" + i + postfix for i in use_summaries]
        elif len(output_file_names) != len(use_summaries):
            output_file_names = [output_file_names[i%len(output_file_names)] + "_" + use_summaries[i] + postfix for i in range(len(use_summaries))]


        for i in range(len(use_summaries)):
            x = list(chain.from_iterable(
                (self.summaries[use_summaries[i]][l][const.TIME], self.summaries[use_summaries[i]][l+1][const.TIME]
                if l+1 < len(self.summaries[use_summaries[i]]) else self.summaries[use_summaries[i]][l][const.TIME])  for l in range(len(self.summaries[use_summaries[i]]))))
            y, all = self._transform_cpu_used(self.summaries[use_summaries[i]])

            fig, ax = plt.subplots()

            c = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00", "#FF00FF", "#00FFFF"]
            # pal = sns.color_palette("Set1")
            pal = self.create_color_palette(c)
            stacks = ax.stackplot(x, y, colors=pal)
            ax.legend(loc='upper right')
            ax.plot(x, all, 'k')

            plt.xlim(0, 25)

            ax.set(xlabel='Time[s]', ylabel='CPUs')

            # hatches = ["","","","","/","//"]
            # for stack, hatch in zip(stacks, cycle(hatches)):
            #     stack.set_hatch(hatch)


            plt.savefig(output_file_names[i])

            # plt.show()
            plt.close("all")

        # x = range(1, 6)
        # y = [[10, 4, 6, 5, 3], [12, 2, 7, 10, 1], [8, 18, 5, 7, 6]]
        #
        # pal = ["#9b59b6", "#e74c3c", "#34495e", "#2ecc71"]
        # plt.stackplot(x, y, labels=['A', 'B', 'C'], colors=pal, alpha=0.4)
        # plt.legend(loc='upper right')
        #
        # plt.show()

    def _transform_cpu_used(self, summary):
        TYPE = 0
        GEN = 1
        TIME = 2

        y = [[[]] for typ in range(const.COUNT_TYPE_OF_INDIVIDUAL)]
        # y = [[[]] for typ in range(2)]
        all = []

        for time in range(len(summary)):
            log = summary[time]
            for typ in range(const.COUNT_TYPE_OF_INDIVIDUAL):
            # for typ in range(2):
                typ_name = const.LIST_TYPE_OF_INDIVIDUAL[typ]

                # {1:1, 3:2} -> [0,1,0,2]
                l = []
                if log[typ_name]:
                    l = [log[typ_name][str(k)] if str(k) in log[typ_name] else 0 for k in range(max(map(int, log[typ_name].keys())) + 1)]

                # add missing generations
                # přidání chybějících generací
                while len(y[0]) < len(l):
                    for t in range(len(y)):
                        if t < typ:
                            y[t].append([0] * (time+1) * 2)
                        else:
                            y[t].append([0] * time * 2)

                # add values
                # přidání hodnot
                for i in range(len(y[typ])):
                    if i < len(l):
                        y[typ][i].append(l[i])
                        y[typ][i].append(l[i])
                    else:
                        y[typ][i].append(0)
                        y[typ][i].append(0)

        # 3D (type, generation, timestamp) -> 2D array, so that the same generation is together
        # 3D (typ, generace, čas)-> 2D pole, tak aby stejná generace byla u sebe
        y2 = []
        for g in range(len(y[0])):
            # for t in range(len(y)):
            # we want only non-speculative and speculative-good individuals
            # chci jen normalni a dobre spekulativni
            for t in range(2):
                y2.append(y[t][g])

        all = np.sum(np.sum(y, axis=0), axis=0).tolist()

        # y = [gens for typ in y for gens in typ]

        # print(y2)

        return y2, all

    def create_color_palette(self, color_list):
        c = []
        step = 1/(const.COUNT_TYPE_OF_INDIVIDUAL+1)
        # count = [2,3,5,4,1,0]
        count = [2,4]
        for color in color_list:
            rgb = colors.to_rgb(color)
            hsv = colors.rgb_to_hsv(rgb)
            for i in count:
                s = 1-i*step
                v = i*(step+1)
                c.append(colors.to_hex(colors.hsv_to_rgb([hsv[0], 1-i*step, (i+1)*step])))
        return c


class PlottingUtilization:
    def __init__(self, dir, algs, ffs, tfs):
        self.dir = dir
        self.algs = algs
        self.ffs = ffs
        self.tfs = tfs
        self.data = pd.DataFrame(columns=["alg", "cpu", "run", "util"])

        self.mu = "m"
        self.lambda_ = "l"
        self.try_get_mu_lambda()

    def try_get_mu_lambda(self):
        splits = [x for x in self.dir.split("/") if (not x.endswith("function")) and (not x.isdigit())]
        root_dir = splits[-1]
        splits2 = [x for x in root_dir.split("_") if x.isdigit()]
        if len(splits2) >= 2:
            self.mu = splits2[0]
            self.lambda_ = splits2[1]

    def get_logs_of_runs(self, dir, alg, ff, tf, cpu):
        paths = []
        if not cpu.isdigit():
            return paths
        # main_path = f"{dir}/{alg}/{tf}/{ff}/{cpu}"
        main_path = os.path.join(dir,alg,tf,ff,cpu)
        for file in os.listdir(main_path):
            if file.endswith("_summary.json"):
                paths.append(file)
        return paths

    def compute_utilization(self, path, useful = True):
        try:
            file = open(path, "r")
            summary = json.load(file)
            file.close()
        except:
            print()
            print("Chyba!")
            print(file)
            print()
            return np.nan

        time = []
        used_cpu = []
        total_cpu = -1

        def get_num_of_cpu(value):
            # return sum([sum(value.values()) for col in value if col])
            filtered = value.filter(items=[col for col in value.index.tolist() if col.startswith("computing")]).tolist()
            free_cpu = value["free_cpu"]
            sum_total = sum([sum(v.values()) for v in filtered]) + free_cpu
            return sum_total

        def get_good_num_of_cpu(value):
            # return sum([sum(value.values()) for col in value if col])
            filtered = value.filter(items=[col for col in value.index.tolist() if col in (const.LIST_TYPE_OF_INDIVIDUAL[0], const.LIST_TYPE_OF_INDIVIDUAL[1])]).tolist()
            free_cpu = value["free_cpu"]
            sum_total = sum([sum(v.values()) for v in filtered]) + free_cpu
            return sum_total

        # st = datetime.datetime.now()
        # tmp = pd.DataFrame.from_records(summary)
        # tmp = tmp.sort_values(by="time")
        # tmp["end_time"] = tmp["time"].shift(-1).ffill()
        # tmp["duration"] = tmp["end_time"] - tmp["time"]
        # tmp["usage_cpus"] = tmp.apply(get_num_of_cpu, axis=1)
        # tmp["good_cpus"] = tmp.apply(get_good_num_of_cpu, axis=1)
        # tmp["total_cpus"] = tmp["usage_cpus"].max()
        # if useful:
        #     wm_numpy = np.average(tmp['good_cpus'], weights=tmp['duration'])
        # else:
        #     wm_numpy = np.average(tmp['usage'], weights=tmp['duration'])
        #
        # tmp_time = datetime.datetime.now() - st
        # st = datetime.datetime.now()

        for s in summary:
            if total_cpu == -1:
                total_cpu = s[const.FREE_CPU]
                for t in const.LIST_TYPE_OF_INDIVIDUAL:
                    total_cpu += sum(s[t].values())

            time.append(s[const.TIME])
            if useful:
                used_cpu.append(sum(s[const.LIST_TYPE_OF_INDIVIDUAL[0]].values()) + sum(s[const.LIST_TYPE_OF_INDIVIDUAL[1]].values()))
            else:
                used_cpu.append(total_cpu - s[const.FREE_CPU])

        time.append(time[-1])
        time_diff = [time[i+1] - time[i] for i in range(len(time)-1)]
        weighted_sum = sum([time_diff[i] * used_cpu[i] for i in range(len(time_diff))])
        wm = weighted_sum/sum(time_diff)/total_cpu
        # actual_time = datetime.datetime.now() - st
        # print(actual_time, wm)
        return wm


    def rename_alg(self, value):
        if value in const.ALG_RENAME_MAP:
            return const.ALG_RENAME_MAP[value].format(mu=self.mu, lambda_ = self.lambda_)
        return value


    def create_dataset(self, useful=True):
        # cur = os.listdir(".")
        # up = os.listdir("..")
        # logs = os.listdir("../logs")
        for alg in self.algs:
            for ff in self.ffs:
                for tf in self.tfs:
                    # main_path = f"{self.dir}/{alg}/{tf}/{ff}"
                    main_path = os.path.join(self.dir,alg,tf,ff)
                    for cpu in os.listdir(main_path):
                        if not os.path.isdir(os.path.join(main_path,cpu)): continue

                        logs = self.get_logs_of_runs(self.dir, alg, ff, tf, cpu)
                        for log in logs:
                            file_name = os.path.basename(log)
                            run = file_name.split("-")[1].split("_")[0]
                            self.data.loc[len(self.data)] = [alg, int(cpu), int(run), self.compute_utilization(os.path.join(main_path,cpu,log), useful)]

        self.data["alg"] = self.data["alg"].apply(self.rename_alg)

    def create_plot(self, output_file_name, useful=True):
        self.create_dataset(useful)
        ax = sns.tsplot(self.data, time="cpu", value="util", unit="run", condition="alg", ci=50)
        ax.set(xlabel='CPUs', ylabel='Utilization')
        # plt.show()
        # self.data.to_csv("result.csv")
        # print(self.data.dtypes)

        plt.savefig(output_file_name)

        plt.close("all")


class PlottingFitness:
    def __init__(self, dir, algs, ff, tf, cpu):
        self.dir = dir
        self.algs = algs
        self.ff = ff
        self.tf = tf
        self.cpu = cpu
        self.data = pd.DataFrame(columns=["alg", "time", "run", "ff"])
        self.times = []

        self.mu = "m"
        self.lambda_ = "l"
        self.try_get_mu_lambda()

    def try_get_mu_lambda(self):
        splits = [x for x in self.dir.split("/") if (not x.endswith("function")) and (not x.isdigit())]
        root_dir = splits[-1]
        splits2 = [x for x in root_dir.split("_") if x.isdigit()]
        if len(splits2) >= 2:
            self.mu = splits2[0]
            self.lambda_ = splits2[1]

    def get_logs_of_runs(self, alg):
        paths = []
        # main_path = f"{self.dir}/{alg}/{self.tf}/{self.ff}/{self.cpu}"
        main_path = os.path.join(self.dir,alg,self.tf,self.ff,str(self.cpu))
        for file in os.listdir(main_path):
            if file.endswith("_summary.json"):
                paths.append(file)
        return paths

    def get_all_times(self, alg):
        # for alg in self.algs:
        # print(datetime.datetime.now(), "Starting creating all_times...")
        self.times = []
        # main_path = f"{self.dir}/{alg}/{self.tf}/{self.ff}/{self.cpu}"
        main_path = os.path.join(self.dir,alg,self.tf,self.ff,str(self.cpu))
        logs = self.get_logs_of_runs(alg)
        for log in logs:
            run = log.split("-")[1].split("_")[0]
            path = f"{main_path}/{log}"
            file = open(path, "r")
            summary = json.load(file)
            file.close()
            for s in summary:
                if not s[const.BEST_FITNESS]: continue
                self.times.append(s[const.TIME])
        self.times = sorted(list(set(self.times)))
        # print(datetime.datetime.now(), "Done creating all_times...")


    def create_dataset(self):
        # print(datetime.datetime.now(), "Starting creating dataset...")
        for alg in self.algs:
            # print(datetime.datetime.now(), f"Starting creating dataset from alg {alg}...")
            self.get_all_times(alg)
            # main_path = f"{self.dir}/{alg}/{self.tf}/{self.ff}/{self.cpu}"
            main_path = os.path.join(self.dir,alg,self.tf,self.ff,str(self.cpu))
            logs = self.get_logs_of_runs(alg)
            for log in logs:
                run = log.split("-")[1].split("_")[0]
                path = f"{main_path}/{log}"
                file = open(path, "r")
                summary = json.load(file)
                file.close()
                last_ff = np.nan
                time_idx = 0
                tmp_data = {"alg":[], "time":[], "run":[], "ff":[]}
                for s in summary:
                    if not s[const.BEST_FITNESS]: continue
                    while self.times[time_idx] < s[const.TIME]:
                        self.data.loc[len(self.data)] = [alg, float(self.times[time_idx]), int(run), float(last_ff)]
                        time_idx += 1
                    if time_idx >= len(self.times):
                        print("over")
                    self.data.loc[len(self.data)] = [alg, float(self.times[time_idx]), int(run), float(s[const.BEST_FITNESS][0])]
                    last_ff = s[const.BEST_FITNESS][0]
                    time_idx += 1
        self.data["ff"] = pd.to_numeric(self.data["ff"])
        self.data["time"] = pd.to_numeric(self.data["time"])
        self.data["run"] = pd.to_numeric(self.data["run"])
        # print(datetime.datetime.now(), "Done creating dataset...")

    def data_fill(self, df, group_by, time):
        # print(datetime.datetime.now(), f"Starting filling dataset...")
        time_serie = df[[time]].drop_duplicates()
        time_serie.set_index(keys=[time], drop=False, inplace=True)
        # sort the dataframe
        df.sort_values(by=[group_by, time], inplace=True)
        # set the index to be this and don't drop
        df.set_index(keys=[group_by], drop=False, inplace=True)
        # get a list of names
        groups = df[group_by].unique().tolist()
        # now we can perform a lookup on a 'view' of the dataframe
        dfs = [df[df[group_by] == group] for group in groups]

        results = []
        for d in dfs:
            res = d.set_index(keys=[time], drop=False)
            res = time_serie.join(res, lsuffix="_l")
            res = res.drop([c for c in res.columns if c.endswith("_l")], axis=1)
            # res = res.loc[:res.last_valid_index()]
            res = res.fillna(method="ffill")
            # remove all values before last none (we want to plot graph when all runs )
            res = res.fillna(method="bfill")

            res[time] = res.index
            results.append(res)

        result = pd.concat(results)


        return result

    def data_prepare(self, df, group_by, time, event):
        # print(datetime.datetime.now(), f"Starting expanding dataset...")
        # sort the dataframe
        df.sort_values(by=[group_by, time], inplace=True)
        # set the index to be this and don't drop
        df.set_index(keys=[group_by], drop=False, inplace=True)
        # get a list of names
        groups = df[group_by].unique().tolist()
        # now we can perform a lookup on a 'view' of the dataframe
        dfs = [df[df[group_by] == group] for group in groups]

        results = []
        for d in dfs:
            res = self.data_fill(d, event, time)
            results.append(res)

        result_df = pd.concat(results).reset_index(drop=True)

        return result_df

    def data_prepare2(self, df, group_by, time, event):
        time_serie = df[[time]].drop_duplicates()
        time_serie.set_index(keys=[time], drop=False, inplace=True)
        # sort the dataframe
        df.sort_values(by=[group_by, event, time], inplace=True)
        # set the index to be this and don't drop
        df.set_index(keys=[group_by, event], drop=False, inplace=True)
        # get a list of names
        groups = df[[group_by, event]].drop_duplicates().values
        # now we can perform a lookup on a 'view' of the dataframe
        dfs = [df[(df[group_by] == group[0]) & (df[event] == group[1])] for group in groups]

        results = []
        for d in dfs:
            res = d.set_index(keys=[time], drop=False)
            res = time_serie.join(res, lsuffix="_l")
            res = res.drop([c for c in res.columns if c.endswith("_l")], axis=1)
            res = res.loc[:res.last_valid_index()]
            res = res.fillna(method="ffill")
            res[time] = res.index
            results.append(res)

        return pd.concat(results).reset_index(drop=True)

    def rename_alg(self, value):
        if value in const.ALG_RENAME_MAP:
            return const.ALG_RENAME_MAP[value].format(mu=self.mu, lambda_ = self.lambda_)
        return value

    def create_dataset2(self):
        # print(datetime.datetime.now(), "Starting creating dataset...")
        for alg in self.algs:
            # print(datetime.datetime.now(), f"Starting creating dataset from alg {alg}...")
            # self.get_all_times(alg)
            # main_path = f"{self.dir}/{alg}/{self.tf}/{self.ff}/{self.cpu}"
            main_path = os.path.join(self.dir,alg,self.tf,self.ff,str(self.cpu))
            logs = self.get_logs_of_runs(alg)
            for log in logs:
                run = log.split("-")[1].split("_")[0]
                path = f"{main_path}/{log}"
                file = open(path, "r")
                summary = json.load(file)
                file.close()
                # last_ff = np.nan
                # for s in summary:
                #     if not s[const.BEST_FITNESS]: continue
                #     self.data.loc[len(self.data)] = [alg, float(s[const.TIME]), int(run),
                #                                      float(s[const.BEST_FITNESS][0])]

                tmp = pd.DataFrame.from_records(summary)
                tmp = tmp[["time", "best_fitness"]]
                tmp = tmp.rename(columns={"best_fitness":"ff"})
                tmp["run"] = int(run)
                tmp["alg"] = alg
                def get_first(x):
                    if not x:
                        return None
                    return x[0]
                tmp["ff"] = list(map(get_first, tmp["ff"]))
                max_time = tmp["time"].max()
                max_time_pow_10 = math.pow(10, math.ceil(math.log10(max_time)))
                max_ticks = 10_000. # real max = #runs * # algs * max_tinks
                rounding = max_time_pow_10/max_ticks
                tmp["time"] = (tmp["time"]/rounding).round()*rounding
                tmp = tmp.groupby("time").max().reset_index()
                tmp = tmp.dropna(subset=["ff"])

                self.data = pd.concat([self.data, tmp])

        self.data["ff"] = pd.to_numeric(self.data["ff"])
        self.data["time"] = pd.to_numeric(self.data["time"])
        self.data["run"] = pd.to_numeric(self.data["run"])

        self.data["alg"] = self.data["alg"].apply(self.rename_alg)

        self.data = self.data_prepare(self.data, "alg", "time", "run")

        # print(datetime.datetime.now(), "Done creating dataset...")

    def create_plot(self, output_file_name):
        # self.get_all_times()
        # print(datetime.datetime.now(), "Starting creating plot...")
        self.create_dataset2()
        # print(self.data)
        ax = sns.tsplot(self.data, time="time", value="ff", unit="run", condition="alg", estimator=np.nanmean, ci=50)
        plt.ylim(0, None)
        ax.set(xlabel='Time [s]', ylabel='Fitness')
        plt.savefig(output_file_name)
        plt.close("all")

        # log scale
        path, file_name = os.path.split(output_file_name)
        log_output_file_name = os.path.join(path, "log_" + file_name)
        ax = sns.tsplot(self.data, time="time", value="ff", unit="run", condition="alg", estimator=np.nanmean, ci=50)
        ax.set_yscale('log')
        ax.set(xlabel='Time [s]', ylabel='Fitness')
        plt.savefig(log_output_file_name)
        plt.close("all")
        # plt.show()
        # self.data.to_csv("result.csv")