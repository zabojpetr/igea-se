from AsyncEA import utils, const
import json
import os

class StatsLog:
    def __init__(self, dir=None, name_postfix="", weights=None, algorithm=None, planner=None, settings=None):
        self.log_name = utils.create_log_name(dir=dir, postfix=name_postfix, extension="txt")
        self.log_name_speculative = utils.create_log_name(dir=dir, postfix=name_postfix+"_speculative", extension="txt")
        os.makedirs(os.path.dirname(self.log_name), exist_ok=True)
        self.log = open(self.log_name,"w")
        self.log_speculative = open(self.log_name_speculative,"w")

        self._write_settings(weights, algorithm, planner, settings)

    def _write_settings(self, weights, algorithm, planner, settings):
        if not settings: settings = {}
        if planner:
            settings[const.N_CPUS] = planner.parallel_pool.n_cpus
            settings[const.ENVIRONMENT] = planner.parallel_pool.__class__.__name__
            settings[const.USE_ESTIMATE] = planner.use_estimate
            settings[const.ABORTABLE] = planner.abortable.name
            try:
                settings[const.GOOD_PARENT_PB] = planner.parallel_pool.good_parent_pb
            except:
                pass
            try:
                settings[const.GOOD_TOURNAMENT_PB] = planner.parallel_pool.good_tournament_pb
            except:
                pass
        if weights:
            settings[const.WEIGHTS] = weights
        if algorithm:
            settings[const.ALGORITHM] = algorithm
        if settings:
            json.dump(settings,self.log)
            self.log.write("\n")
        self.log.write("\n")
        self.log.flush()

    def planner_log(self, task):
        log = {}
        log[const.ID] = task.id
        log[const.START_TIME] = task.start_time.total_seconds()
        log[const.END_TIME] = task.end_time.total_seconds()
        log[const.CPU] = task.cpu
        log[const.VALUE] = task.value
        log[const.SPECULATIVE] = str(task.speculative)
        log[const.TERMINATE_TIME] = task.terminate_time.total_seconds()
        log[const.TERMINATED] = str(task.terminated)
        log[const.GENERATION] = task.generation
        log[const.INDIVIDUAL] = task.individual
        json.dump(log,self.log)
        self.log.write("\n")
        self.log.flush()

    def planner_log_end(self, task, end_time):
        task.end_time = end_time
        self.planner_log(task)


    def algorithm_log(self, id, speculative):
        log = {}
        log[const.ID] = id
        log[const.SPECULATIVE] = str(speculative)
        json.dump(log,self.log_speculative)
        self.log_speculative.write("\n")
        self.log_speculative.flush()