import json
import os
import datetime
from operator import itemgetter
import heapq
import sys
import random
from AsyncEA import const
from AsyncEA import utils
from collections import Counter
from AsyncEA.Task import *

class LogProcessing:
    def __init__(self, logs, n_cpus):
        self.filename_prefix = ""
        self.logs = logs
        self.n_cpus = n_cpus
        self.summary = None
        self.weights = []

    @classmethod
    def from_log_file(cls, filename):
        try:
            file = open(filename, "r")
        except FileNotFoundError:
            print("File not found!")
            return None


        def json_loads(line):
            try:
                return json.loads(line)
            except:
                return None

        speculative = dict()
        # read file with speculative results
        # nacteni speculative souboru
        try:
            speculative_file = open(os.path.splitext(filename)[0] + "_speculative.txt", "r")
            lines = list(map(json_loads, speculative_file.readlines()))
            for l in lines:
                if l:
                    speculative[l[const.ID]] = l[const.SPECULATIVE]
            speculative_file.close()
        except FileNotFoundError:
            print("File with speculative results not found!")

        settings = []
        line = file.readline()
        while(line != "\n"):
            l = json_loads(line)
            if l!=None:
                settings.append(l)
            line = file.readline()

        n_cpus = 0
        for i in settings:
            if(const.N_CPUS in i):
                n_cpus = i[const.N_CPUS]
            if(const.WEIGHTS in i):
                weights = i[const.WEIGHTS]


        logs = list(map(json_loads, file.readlines()))

        # I can read file, which is in use for writing -> last record can be invalid
        # Mohu číst soubor, do kterého se aktuálně zapisuje -> poslední záznam je vadný
        if logs[-1] == None:
            del logs[-1]

        if None in logs:
            print("Bad format of file!")
            return None


        for l in logs:
            if l[const.ID] in speculative:
                l[const.SPECULATIVE] = speculative[l[const.ID]]

        c = cls(logs, n_cpus)
        c.filename_prefix = os.path.splitext(filename)[0]
        c.weights = weights


        file.close()

        return c

    def create_summary(self, output_file_name=None, create_file=True):
        if output_file_name == None:
            if(self.filename_prefix == None):
                self.filename_prefix = utils.create_log_name(datetime.datetime.now())
            output_file_name = self.filename_prefix + "_summary.json"
        if create_file:
            f = open(output_file_name, "w")

        self.logs = sorted(self.logs, key = itemgetter(const.START_TIME, const.END_TIME))

        running = []

        summary = []

        time = 0
        best_fitness = []
        evals = 0
        total_eval_time = 0

        for log in self.logs:

            # print("Running:")
            # print(running)
            # remove all runing tasks which ended in this time and switch tasks after retmination
            # odebrat všechny běžící tasky, co končí v tomto čase a přepnou tasky po terminaci
            while(running and running[0][0] <= log[const.START_TIME]):
                if running[0][0] == self.get_last_time(running[0][2][const.TERMINATE_TIME], running[0][2][const.END_TIME]):
                    ended = heapq.heappop(running)[2]
                    time = ended[const.END_TIME]
                    best_fitness = self.get_best_fitness(best_fitness, ended[const.VALUE])
                    evals += 1
                    total_eval_time += (ended[const.END_TIME]-ended[const.START_TIME])
                else:
                    new = (running[0][2][const.END_TIME], random.random(),running[0][2])
                    ended = heapq.heapreplace(running, new)[2]
                    time = ended[const.TERMINATE_TIME]

                l = self.create_summary_log(time, running, best_fitness, evals, total_eval_time)

                # we want to write only the last record with one timestamp
                #chceme zapsat jen poslední údaj s jedním časem
                if(not summary or summary[-1][const.TIME] != time):
                    summary.append(l)
                else:
                    summary[-1] = l



            heapq.heappush(running,(self.get_min_time(log[const.TERMINATE_TIME], log[const.END_TIME]), random.random(), log))
            time = log[const.START_TIME]
            l = self.create_summary_log(time, running, best_fitness, evals, total_eval_time)

            # we want to write only the last record with one timestamp
            # chceme zapsat jen poslední údaj s jedním časem
            if (not summary or summary[-1][const.TIME] != time):
                summary.append(l)
            else:
                summary[-1] = l


        while(running):
            if running[0][0] == self.get_last_time(running[0][2][const.TERMINATE_TIME], running[0][2][const.END_TIME]):
                ended = heapq.heappop(running)[2]
                time = ended[const.END_TIME]
                best_fitness = self.get_best_fitness(best_fitness, ended[const.VALUE])
                evals += 1
                total_eval_time += (ended[const.END_TIME] - ended[const.START_TIME])
            else:
                new = (running[0][2][const.END_TIME], random.random(),running[0][2])
                ended = heapq.heapreplace(running, new)[2]
                time = ended[const.TERMINATE_TIME]

            l = self.create_summary_log(time, running, best_fitness, evals, total_eval_time)

            # we want to write only the last record with one timestamp
            # chceme zapsat jen poslední údaj s jedním časem
            if (not summary or summary[-1][const.TIME] != time):
                summary.append(l)
            else:
                summary[-1] = l

        self.summary = summary

        if create_file:
            json.dump(summary,f, indent="   ")
            f.flush()
            f.close()

    def create_summary_log(self, time, running, best_fitness, evals, total_eval_time):
        LOG = 2
        log = {}
        log[const.TIME] = time
        log[const.FREE_CPU] = self.n_cpus - len(running) if self.n_cpus != 0 else 0
        log["computing_gens"] = Counter([x[LOG][const.GENERATION] for x in running if x[LOG][const.SPECULATIVE] == str(Speculative.NONE)])
        log["computing_good_speculative_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                          x[LOG][const.SPECULATIVE] == str(Speculative.GOOD) and (
                                                                  x[LOG][const.TERMINATE_TIME] > time or x[LOG][
                                                              const.TERMINATE_TIME] == -1)])
        log["computing_bad_speculative_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                         x[LOG][const.SPECULATIVE] == str(Speculative.BAD) and (
                                                                 x[LOG][const.TERMINATE_TIME] > time or x[LOG][
                                                             const.TERMINATE_TIME] == -1)])
        log["computing_unknown_speculative_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                             x[LOG][const.SPECULATIVE] == str(Speculative.UNKNOWN) and (
                                                                     x[LOG][const.TERMINATE_TIME] > time or x[LOG][
                                                                 const.TERMINATE_TIME] == -1)])
        log["computing_terminated_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                    x[LOG][const.TERMINATE_TIME] != -1 and x[LOG][
                                                        const.TERMINATE_TIME] <= time and x[LOG][
                                                        const.TERMINATED] == str(Terminated.REGULAR)])
        log["computing_terminated_bad_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                        x[LOG][const.TERMINATE_TIME] != -1 and x[LOG][
                                                            const.TERMINATE_TIME] <= time and x[LOG][
                                                            const.TERMINATED] == str(Terminated.BAD)])
        log["computing_terminated_crowded_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                        x[LOG][const.TERMINATE_TIME] != -1 and x[LOG][
                                                            const.TERMINATE_TIME] <= time and x[LOG][
                                                            const.TERMINATED] == str(Terminated.CROWDED)])
        log["computing_terminated_bad_position_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                        x[LOG][const.TERMINATE_TIME] != -1 and x[LOG][
                                                            const.TERMINATE_TIME] <= time and x[LOG][
                                                            const.TERMINATED] == str(Terminated.BAD_POSITION)])
        log["computing_terminated_bad_tournament_gens"] = Counter([x[LOG][const.GENERATION] for x in running if
                                                        x[LOG][const.TERMINATE_TIME] != -1 and x[LOG][
                                                            const.TERMINATE_TIME] <= time and x[LOG][
                                                            const.TERMINATED] == str(Terminated.BAD_TOURNAMENT)])
        log["best_fitness"] = best_fitness
        log["evals"] = evals
        log["total_eval_time"] = total_eval_time

        return  log

    def get_min_time(self, terminate, end):
        t = 0
        if terminate != const.MIN_TIME and end != const.MIN_TIME:
            t = min(terminate, end)
        elif terminate == const.MIN_TIME:
            t = end
        else:
            t = terminate
        return t

    def get_last_time(self, terminate, end):
        last = 0
        if terminate != const.MIN_TIME and end != const.MIN_TIME:
            last = max(terminate, end)
        elif terminate == const.MIN_TIME:
            last = end
        else:
            last = terminate
        return last

    def get_best_fitness(self, fitness1, fitness2):
        for i in range(min(len(fitness1), len(fitness2))):
            if self.weights and len(self.weights) > i:
                if self.weights[i] * fitness1[i] > self.weights[i] * fitness2[i]:
                    return fitness1
                elif self.weights[i] * fitness1[i] < self.weights[i] * fitness2[i]:
                    return fitness2
            else:
                if fitness1[i] > fitness2[i]:
                    return fitness1
                elif fitness1[i] < fitness2[i]:
                    return fitness2

        if len(fitness1) >= len(fitness2):
            return fitness1
        else:
            return fitness2

